<?php

/*
 * ********************************************************************
 * Class Model Data_model [ci class model]
 * oleh     : Christiantinus Nesi
 * email    : christiantinusnesi@gmail.com
 * tahun    : 2018
 * ********************************************************************
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Data_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function simpanData($data, $tabel) {
        $this->db->insert($tabel, $data);
    }

    public function updateData($id, $data, $tabel, $field = 'tid') {
        $this->db->where($field, $id);
        $this->db->update($tabel, $data);
    }

    public function updateDataWhere($data, $tabel, $where) {
        $this->db->where($where);
        $this->db->update($tabel, $data);
    }

    public function hapusData($id, $tabel, $field = 'tid') {
        $this->db->where($field, $id);
        $this->db->delete($tabel);
    }

    public function hapusDataWhere($tabel, $where) {
        $this->db->where($where);
        $this->db->delete($tabel);
    }

    public function ambilData($id, $tabel, $field = 'tid') {
        $this->db->from($tabel);
        if ($field <> 'tid') {
            $this->db->where($field, $id);
        }
        $getData = $this->db->get();

        if ($getData->num_rows() > 0) {
            return $getData->row();
        } else {
            return false;
        }
    }

    public function ambilDataWhere($tabel, $where, $orderby, $ascdesc, $grp = '', $select = '') {
        if ($select <> '') {
            $this->db->select($select);
        }
        $this->db->from($tabel);
        $this->db->where($where);
        if ($grp <> '') {
            $this->db->group_by($grp);
        }
        $this->db->order_by($orderby, $ascdesc);
        $getData = $this->db->get();
        if ($getData->num_rows() > 0) {
            return $getData->result();
        } else {
            return false;
        }
    }

    public function satuData($tabel, $where) {
        $this->db->from($tabel);
        $this->db->where($where);
        $getData = $this->db->get();

        if ($getData->num_rows() > 0) {
            return $getData->row();
        } else {
            return false;
        }
    }

    public function cekData($tabel, $where = "") {
        $this->db->from($tabel);
        if ($where <> "") {
            $this->db->where($where);
        }
        $getData = $this->db->get();
        return $getData->num_rows();
    }

    public function selectData($tabel, $orderby, $where = "", $ascdesc = 'asc') {
        $this->db->from($tabel)->order_by($orderby, $ascdesc);
        if ($where <> "") {
            $this->db->where($where);
        }
        $getData = $this->db->get();
        if ($getData->num_rows() > 0) {
            return $getData->result();
        } else {
            return false;
        }
    }

    public function getLastIdDb($tabel, $key, $where = '') {
        $querynya = "SELECT * FROM " . $tabel . $where . " ORDER BY " . $key . " DESC LIMIT 1";
        $query_result = $this->db->query($querynya);
        $data_last = $query_result->result_array();
        return ($data_last) ? $data_last[0] : false;
    }

    public function getLastChild($tabel, $key, $field) {
        $querynya = "SELECT $field FROM $tabel WHERE parent=$key ORDER BY $field DESC LIMIT 1";
        $query_result = $this->db->query($querynya);
        $data_last = $query_result->row();
        return ($data_last) ? $data_last->$field : 0;
    }

    public function jalankanQuery($query, $return = '') {
        $res = $this->db->query($query);
        if ($return == 1) {
            $result = $res->row();
        } elseif ($return == 2) {
            $pre = $res->row();
            $result = ($pre) ? $pre->a : 0;
        } elseif ($return == 3) {
            $result = $res->result();
        } elseif ($return == 4) {
            $result = $res->num_rows();
        } else {
            $result = $res;
        }
        return $result;
    }

    public function verify_user($condition, $tabel) {
        $q = $this
                ->db
                ->where($condition)
                ->limit(1)
                ->get($tabel);

        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    public function perPage() {
        return 5;
    }

    public function getTotalData($tabel, $where) {
        $this->db->from($tabel);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getList($limit, $offset, $where, $kolom, $order, $tabel) {
        $this->db->limit($limit, $offset);
        $this->db->from($tabel);
//        $this->db->select("id, title, alias, title_alias, introtext, `fulltext`, state, sectionid, mask, catid, created, created_by, images, urls, parentid, ordering,publish_up");
        $this->db->where($where);
        $this->db->order_by($kolom, $order);
        $q = $this->db->get();
        return $q->result();
    }

    public function getProsedur($query) {
        $qry_res = $this->db->query($query);
        $res = $qry_res->result_array();
        $qry_res->next_result();
        $qry_res->free_result();

        if (count($res) > 0) {
            return $res;
        } else {
            return 0;
        }
    }

    public function load_roster_ta($date, $perner_id)
    {                   
        $db_garuda = $this->load->database('db_garuda', TRUE);            
        $db_garuda->select('a.tgl, a.perner_id, DAYOFWEEK(a.tgl) AS hari, a.shifting_code, b.shifting_work_hour, c.ist_interval AS ist_1_hour, d.ist_interval AS ist_2_hour, e.ist_interval AS ist_3_hour, a.ws, f.off_desc');
        $db_garuda->from('roster_schedule a');
        $db_garuda->join('roster_shifting as b', 'a.shifting_code = b.shifting_code', 'left');
        $db_garuda->join('roster_istirahat c', 'a.ist_1 = c.ist_code', 'left');
        $db_garuda->join('roster_istirahat d', 'a.ist_2 = d.ist_code', 'left');
        $db_garuda->join('roster_istirahat e', 'a.ist_3 = e.ist_code', 'left');
        $db_garuda->join('roster_off f', 'a.shifting_code = f.off_code', 'left');
        $db_garuda->where('DATE_FORMAT(a.tgl,\'%Y-%m\')', $date);
        $db_garuda->where('a.perner_id', $perner_id);
        $db_garuda->where('a.published', '1');
        $db_garuda->order_by('a.tgl', 'asc');
                                
        return $db_garuda->get();
    }

    function load_data_sdm($pos)
    {       
        $db_sdm = $this->load->database('db_sdm', TRUE);
        
        $db_sdm->select('a.perner_id, a.sdm_name');
        $db_sdm->from('sdm a');     
        $db_sdm->where('state_id', '1');
        
        if ($pos) {
            $db_sdm->where('position', $pos);
        }
        
        $db_sdm->order_by('a.sdm_name','asc');
        
        return $db_sdm->get()->result();
    }

}

?>