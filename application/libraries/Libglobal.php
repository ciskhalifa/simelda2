<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
define('STATUS_ACTIVATED', '1');
define('STATUS_NOT_ACTIVATED', '0');

class Libglobal
{

    function getMenuID($menu, $role)
    {
        $CI = &get_instance();
        $res = $CI->db->select('a.*,b.tambah,b.ubah,b.hapus,b.fitur,b.kode')
            ->from('t_menu a')
            ->join('t_role_menu b', 'a.kode=b.menu')
            ->where('link_menu', $menu)
            ->where('role', $role)
            ->get();
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return FALSE;
        }
    }

    function pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex = '')
    {

        $CI = &get_instance();
        $aColumns = $aColumns;
        $sIndexColumn = $sIndexColumn;
        $sTable = $sTable;
        $sLimit = 0;
        $sOrder = "";

        $xDisplayStart = $CI->input->post('iDisplayStart');
        $xDisplayLength = $CI->input->post('iDisplayLength');
        if (($xDisplayStart != '') && $xDisplayLength != '-1') {
            $sLimit = ($xDisplayStart == 0) ? $xDisplayLength : (($xDisplayStart) + 10);
        }

        $xOrder = $CI->input->post('iSortCol_0');
        $xSortingCols = $CI->input->post('iSortingCols');

        if (isset($xOrder)) {
            $sOrder0 = "ORDER BY  ";
            for ($i = 0; $i < intval($xSortingCols); $i++) {
                $xSortCol = $CI->input->post('iSortCol_' . $i);
                $xSortDir = $CI->input->post('sSortDir_' . $i);
                $xSortable = $CI->input->post('bSortable_' . intval($xSortCol));
                if ($xSortable == "true") {
                    $sOrder0 .= $aColumns[intval($xSortCol)] . " " . $xSortDir . ", ";
                }
            }
            $sOrder = substr_replace($sOrder0, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
            
        }

        $sWhere = "";
        $xSearch = ($CI->input->get('sSearch') != "") ? $CI->input->get('sSearch') : (($CI->input->post('sSearch')) ? $CI->input->post('sSearch') : '');
        if ($xSearch != "") {
            $sWhere0 = "AND ("; //"WHERE (";
            for ($i = 0; $i < (count($aColumns) - 1); $i++) {
                $sWhere0 .= $aColumns[$i] . " LIKE '%" . $xSearch . "%' OR ";
            }
            $sWhere = substr_replace($sWhere0, "", -3);
            $sWhere .= ')';
        }
        for ($i = 0; $i < (count($aColumns) - 1); $i++) {
            $xSearchable = ($CI->input->get('bSearchable_' . $i) != "") ? $CI->input->get('bSearchable_' . $i) : (($CI->input->post('bSearchable_' . $i)) ? $CI->input->post('bSearchable_' . $i) : '');
            $xSearch = ($CI->input->get('sSearch_' . $i) != "") ? $CI->input->get('sSearch_' . $i) : (($CI->input->post('sSearch_' . $i)) ? $CI->input->post('sSearch_' . $i) : '');
            if ($xSearchable == "true" && $xSearch != '') {
                if ($sWhere === "") : $sWhere = "AND ";
                else : $sWhere .= " AND ";
                endif;
                $sWhere .= "" . $aColumns[($i + 1)] . " LIKE '%" . ($xSearch) . "%' ";
            }
        }
        $nextLimit = (($sLimit - $xDisplayLength) <= 0) ? '' : ($sLimit - $xDisplayLength) . ',';
        $xLimit = $xDisplayLength;

        if ($xDisplayLength == '-1') {
            $sLimit = '';
        } else {
            $sLimit = "LIMIT $nextLimit $xLimit";
        }

        $ssQ = $tQuery . " $sWhere $sOrder $sLimit ";

        $rResult = $CI->db->query($ssQ);
        $sQuery = "SELECT COUNT(*) as aTot FROM ($tQuery) as hafidz WHERE 1=1 $sWhere";
        $rResultFilterTotal = $CI->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->row();
        $iFilteredTotal = $aResultFilterTotal->aTot;

        $sQuery = "SELECT COUNT(" . $sIndexColumn . ") as aTot FROM ($tQuery) as hafidz WHERE 1=1 $sWhere";
        $rResultTotal = $CI->db->query($sQuery);
        $aResultTotal = $rResultTotal->row();
        $iTotal = $aResultTotal->aTot;
        $xEcho = ($CI->input->get('sEcho') != "") ? $CI->input->get('sEcho') : (($CI->input->post('sEcho')) ? $CI->input->post('sEcho') : '');
        $output = array(
            "sEcho" => intval($xEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        $resultx = $rResult->result_array();
        foreach ($resultx as $aRow) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aColumns[$i] == "version") {
                    /* Special output formatting for 'version' column */
                    $row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
                } else if ($aColumns[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }
        return json_encode($output);
    }

    /*
     * trnsformasi tanggal ke format indonesia
     */

    public function date2Ind($str)
    {

        $BulanIndo = array(
            "Januari", "Februari", "Maret",
            "April", "Mei", "Juni",
            "Juli", "Agustus", "September",
            "Oktober", "November", "Desember"
        );

        $array_hari = array(1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu');

        $hari = $array_hari[date("N", strtotime($str))];

        $thn = substr($str, 0, 4);
        $bln = substr($str, 5, 2);
        $tgl = substr($str, 8, 2);
        $jam = (strlen(trim(substr($str, 11, 8))) > 0) ? substr($str, 11, 8) : '';

        $result = $tgl . " " . $BulanIndo[(int) $bln - 1] . " " . $thn; //$hari . " " . . (($jam == '') ? '' : " | " . $jam);
        return $result;
    }

    public function dateReverse($str)
    {
        $BulanIndo = array(
            "Januari" => 1, "Februari" => 2, "Maret" => 3,
            "April" => 4, "Mei" => 5, "Juni" => 6,
            "Juli" => 7, "Agustus" => 8, "September" => 9,
            "Oktober" => 10, "November" => 11, "Desember" => 12
        );

        if (strpos($param, '-')) {
            $arrstr = explode('-', $str);
        } else {
            $arrstr = explode(' ', $str);
        }
        $thn = $arrstr[0];
        $bln = $BulanIndo[$arrstr[1]];
        $tgl = $arrstr[2];
        $result = $tgl . "-" . $bln . "-" . $thn;
        return $result;
    }

    public function umur($dob)
    {
        if (!empty($dob)) {
            $birthdate = new DateTime($dob);
            $today = new DateTime('today');
            $age = $birthdate->diff($today)->y;
            return $age;
        } else {
            return 0;
        }
    }
}
