<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PrintController extends MX_Controller
{
    public function test()
    {
        $mpdf = new \Mpdf\Mpdf();
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vrekomendasi WHERE kode='2'", 3);

        $data = $this->load->view('coaching_print', array('rowdata' => $q), TRUE);
        $name = "COUNSELING_PRINT_TEST.pdf";
        $mpdf->SetTitle($name);
        $mpdf->WriteHTML($data);
        $mpdf->Output($name, 'I');
    }
    public function index()
    {
        $kode = $this->uri->segment(3);
        $jenis = $this->uri->segment(2);

        if ($jenis == "1") {
            $this->downloadCoaching($kode);
        } else if ($jenis == "2") {
            $this->downloadCounseling($kode);
        } else if ($jenis == "3") {
            $this->downloadBATL($kode);
        } else if ($jenis == "4") {
            $this->downloadSP($kode);
        } else { }
    }
    public function downloadCoaching($kode)
    {
        $mpdf = new \Mpdf\Mpdf();
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vbina WHERE kode=" . $kode, 3);
        $data = $this->load->view('coaching_print', array('rowdata' => $q), TRUE);
        $name = "COACHING_PRINT_" . $q[0]->kode . '.pdf';
        $mpdf->SetTitle($name);
        $mpdf->WriteHTML($data);
        $mpdf->Output($name, 'I');
    }
    public function downloadCounseling($kode)
    {
        $mpdf = new \Mpdf\Mpdf();
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vbina WHERE kode=" . $kode, 3);
        $data = $this->load->view('counseling_print', array('rowdata' => $q), TRUE);
        $name = "COUNSELING_PRINT_" . $q[0]->kode . '.pdf';
        $mpdf->SetTitle($name);
        $mpdf->WriteHTML($data);
        $mpdf->Output($name, 'I');
    }
    public function downloadBATL($kode)
    {
        $mpdf = new \Mpdf\Mpdf();
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vbina WHERE kode=" . $kode, 3);
        $data = $this->load->view('batl_print', array('rowdata' => $q), TRUE);
        $name = "BATL_PRINT_" . $q[0]->agent . '.pdf';
        $mpdf->SetTitle($name);
        $mpdf->WriteHTML($data);
        $mpdf->Output($name, 'I');
    }
    public function downloadSP($kode)
    {
        $mpdf = new \Mpdf\Mpdf();
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vbina WHERE kode=" . $kode, 3);
        $data = $this->load->view('sp_print',  array('rowdata' => $q), TRUE);
        $name = "SP_PRINT_" . $q[0]->agent . '.pdf';
        $mpdf->SetTitle($name);
        $mpdf->WriteHTML($data);
        $mpdf->Output($name, 'I');
    }
}
