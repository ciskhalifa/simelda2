<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SendEmailController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.mailtrap.io',
            'smtp_port' => 2525,
            'smtp_user' => 'e3e027a2c741d9',
            'smtp_pass' => '5a1a1ff5773c70',
            'crlf' => "\r\n",
            'newline' => "\r\n"
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->from_email = "support@garudazone.com";
    }

    public function index()
    {
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vrekomendasi WHERE kode_tl=7", 3);
        $this->load->view('coaching_mail_tl', array('rowdata' => $q));
    }

    public function sendEmail($email = "", $kode = "", $jenis = "")
    {
        if ($jenis == "Rekomendasi") {
            $bina = $this->Data_model->jalankanQuery("SELECT * FROM vmailing WHERE tipe='bina'", 3);
            foreach ($bina as $row) {
                if (is_array($row->agent)){
                    $pecah = explode('#', $row->agent);
                }else{
                    $email = $row->email_tl;
                }
                $this->email->clear();
                $this->email->from($this->from_email, 'Simelda APP, Rekomendasi Pembinaan');
                $this->email->to($row->email_tl);
                $this->email->subject('Rekomendasi-' . $row->nama_jenis . '-' . $row->nama_kategori);
                $data = $this->Data_model->jalankanQuery("SELECT * FROM vmailing WHERE kode_tl=" . $row->kode_tl, 3);
                $body = $this->load->view('rekomendasi_tl', array('rowdata' => $data), TRUE);
                $this->email->message($body);
                $this->email->set_mailtype("html");
                if ($this->email->send()) {
                    sleep(5);
                    echo 'Sent email successful';
                } else {
                    echo 'You have encountered an error';
                }
            }
            // $this->db->query("TRUNCATE TABLE mailing");
        } else if ($jenis == "Bina") {
            $bina = $this->Data_model->jalankanQuery("SELECT * FROM vbina WHERE kode=" . $kode['kode'], 3);
            $this->email->from($this->from_email, 'Simelda APP, Pembinaan ' . $bina[0]->nama_kategori);
            $this->email->to($email);
            $this->email->subject('Pembinaan-' . $bina[0]->nama_jenis . '-' . $bina[0]->nama_kategori);
            switch ($bina[0]->kode_jenis):
                case '1':
                    // COACHING
                    $hal = 'coaching_mail_ta';
                    break;
                case '2':
                    // COUNSELING 
                    $hal = 'counseling_mail_ta';
                    break;
                case '3':
                    // BATL
                    $hal = 'batl_mail_ta';
                    break;
                case '4':
                    // SP
                    $hal = 'sp_mail_ta';
                    break;
                case '5':
                    // PPJP
                    $hal = '';
                    break;
                default:
                    $hal = "";
                    break;
            endswitch;
            $body = $this->load->view($hal, array('rowdata' => $bina), TRUE);
            $this->email->message($body);
            $this->email->set_mailtype("html");
            if ($this->email->send()) {
                $this->db->query("TRUNCATE TABLE mailing");
                echo 'Sent email successful';
            } else {
                echo 'You have encountered an error';
            }
        } else {
            $service = $this->Data_model->jalankanQuery("SELECT vbina.*, DATEDIFF(tgl_verifikasi, DATE(NOW())) AS selisih FROM vbina WHERE STATUS ='VERIFIKASI'", 3);
            foreach($service as $row){
                $this->email->from($this->from_email, 'Simelda APP, Alert Pembinaan-' . $row->nama_kategori);
                if ($row->selisih <= 2){
                    if (is_null($row->approval_agent) || is_null($row->action_plan)) {
                        $this->email->to($row->email_agent);
                        $hal = 'alert_mail_ta';
                    }else if (is_null($row->approval_atasan) || is_null($row->hasil_verifikasi)) {
                        $this->email->to($row->email_tl);
                        $hal = 'alert_mail_tl';
                    }else if (is_null($row->approval_spv)) {
                        $this->email->to($row->email_spv);
                        $hal = 'alert_mail_spv';
                    }
                    $this->email->subject('Alert Pembinaan-' . $row->nama_jenis . '-' . $row->nama_kategori);
                    $body = $this->load->view($hal, array('rowdata' => $service), TRUE);
                    $this->email->message($body);
                    $this->email->set_mailtype("html");
                    if ($this->email->send()) {
                        sleep(5);
                        //$this->db->query("TRUNCATE TABLE mailing");
                        echo 'Sent email successful';
                    } else {
                        echo 'You have encountered an error';
                    }
                }else{

                }
            }  
        }
    }
}
