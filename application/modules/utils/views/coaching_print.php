<style>
    .logo {
        height: 70px;
        /* margin: px 10px 10px 10px; */
    }

    .head {
        text-align: center;
    }

    .separator {
        border: 2px solid rgba(0, 0, 0, .1);
    }

    .kotak {
        border: 1px;
    }
</style>

<img src="<?= base_url('publik/logo/infomedia.png') ?>" class="logo">
<div class="head">
    <h2>FORM COACHING</h2>
</div>
<hr>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="card-body">
            <table>
                <tr>
                    <td>Tanggal Pembinaan</td>
                    <td>:</td>
                    <td><?= date('d F Y', strtotime($rowdata[0]->tgl_data)); ?></td>
                </tr>
                <tr>
                    <td>Jenis Coaching</td>
                    <td>:</td>
                    <td><?= $rowdata[0]->nama_subkts; ?></td>
                </tr>
                <tr>
                    <td>Tingkat</td>
                    <td>:</td>
                    <td><?= $rowdata[0]->tingkat; ?></td>
                </tr>
                <tr>
                    <td>Batas Verifikasi</td>
                    <td>:</td>
                    <td><?= date('d F Y', strtotime($rowdata[0]->tgl_verifikasi)); ?></td>
                </tr>
                <tr>
                    <td>Nama SDM</td>
                    <td>:</td>
                    <td><?= $rowdata[0]->agent; ?></td>
                </tr>
                <tr>
                    <td>Atasan Langsung</td>
                    <td>:</td>
                    <td><?= $rowdata[0]->tl; ?></td>
                </tr>

            </table>
        </div>
    </div>
</div>
<hr class="separator">
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <h6 class="m-0 font-weight-bold text-center">PERMASALAHAN</h6>
            <textarea rows="5" cols="100" class="form-control input-lg" style="height:100px"><?= $rowdata[0]->masalah; ?></textarea>
            <h6 class="m-0 font-weight-bold text-center">PENYULUHAN</h6>
            <textarea rows="5" cols="100" class="form-control input-lg" style="height:100px"><?= $rowdata[0]->penyuluhan; ?></textarea>
            <h6 class="m-0 font-weight-bold text-center">ACTION PLAN</h6>
            <textarea rows="5" cols="100" class="form-control input-lg" style="height:100px"><?= $rowdata[0]->action_plan; ?></textarea>
        </div>
    </div>
</div>
<hr class="separator">
<br>
<br>
<table style="width:100%">
    <tr>
        <td colspan="12">Bandung, <?= date('d F Y'); ?></td>
    </tr>
    <tr>
        <td colspan="4">Mengetahui,<br> Atasan Penegur</td>
        <td style="text-align:center;" colspan="4">Pegawai yang ditegur</td>
        <td style="text-align:center;" colspan="4">Pemberi Teguran</td>
    </tr>
    <tr>
        <td colspan="4"><br><br><br><br></td>
        <td colspan="4"><br><br><br><br></td>
        <td colspan="4"><br><br><br><br></td>
    </tr>
    <tr>
        <td colspan="4" rowspan="2"><?= (!is_null($rowdata[0]->approval_spv)) ? date("d F Y", strtotime($rowdata[0]->approval_spv)) : "Not Approved"; ?></td>
        <td style="text-align:center;" colspan="4" rowspan="2"><?= (!is_null($rowdata[0]->approval_agent)) ? date("d F Y", strtotime($rowdata[0]->approval_agent)) : "Not Approved"; ?></td>
        <td style="text-align:center;" colspan="4" rowspan="2"><?= (!is_null($rowdata[0]->approval_atasan)) ? date("d F Y", strtotime($rowdata[0]->approval_atasan)) : "Not Approved"; ?></td>
    </tr>
    
</table>