<style>
    .logo {
        height: 70px;
        /* margin: px 10px 10px 10px; */
    }

    .head {
        text-align: center;
    }

    .separator {
        border: 2px solid rgba(0, 0, 0, .1);
    }

    .kotak {
        border: 1px;
    }
</style>

<img src="<?= base_url('publik/logo/infomedia.png') ?>" class="logo">
<div class="head">
    <h2>Alert Pembinaan</h2>
</div>
<hr>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="card-body">
            <h4>Hi, <?= $rowdata[0]->spv; ?></h4>
            <h5 style="color: red;">Silahkan untuk melakukan approval pembinaan tersebut, dikarenakan batas verifikasi akan berakhir</h5>
            <hr class="separator">
            <h2>Detail Pembinaan</h2>
            <table>
                <tr>
                    <td>Tanggal Pembinaan</td>
                    <td>:</td>
                    <td><?= date('d F Y', strtotime($rowdata[0]->tgl_data)); ?></td>
                </tr>
                <tr>
                    <td>Jenis Coaching</td>
                    <td>:</td>
                    <td><?= $rowdata[0]->nama_subkts; ?></td>
                </tr>
                <tr>
                    <td>Tingkat</td>
                    <td>:</td>
                    <td><?= $rowdata[0]->tingkat; ?></td>
                </tr>
                <tr>
                    <td>Batas Verifikasi</td>
                    <td>:</td>
                    <td><?= date('d F Y', strtotime($rowdata[0]->tgl_verifikasi)); ?></td>
                </tr>
                <tr>
                    <td>Nama SDM</td>
                    <td>:</td>
                    <td><?= $rowdata[0]->agent; ?></td>
                </tr>
                <tr>
                    <td>Atasan Langsung</td>
                    <td>:</td>
                    <td><?= $rowdata[0]->tl; ?></td>
                </tr>
            </table>
            <hr class="separator">
            <table border="1" class="table">
                <tr>
                    <th>PERMASALAHAN</th>
                    <th>PENYULUHAN</th>
                    <th>HASIL VERIFIKASI</th>
                    <th>TTD AGENT</th>
                    <th>TTD SPV</th>
                    <th>TTD TEAM LEADER</th>
                </tr>
                <tr>
                    <td style="text-align:left;"><?= $rowdata[0]->masalah; ?></td>
                    <td style="text-align:left;"><?= $rowdata[0]->penyuluhan; ?></td>
                    <td style="text-align:left;"><?= $rowdata[0]->hasil_verifikasi; ?></td>
                    <td style="text-align:left;"><?= (is_null($rowdata[0]->approval_agent)) ? '' : date('d F Y', strtotime($rowdata[0]->approval_agent)); ?></td>
                    <td style="text-align:left;"><?= (is_null($rowdata[0]->approval_spv)) ? '' : date('d F Y', strtotime($rowdata[0]->approval_spv)); ?></td>
                    <td style="text-align:left;"><?= (is_null($rowdata[0]->approval_atasan)) ? '' : date('d F Y', strtotime($rowdata[0]->approval_atasan)); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<hr class="separator">