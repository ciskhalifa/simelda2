<style>
    .logo {
        height: 70px;
        /* margin: px 10px 10px 10px; */
    }

    .head {
        text-align: center;
    }

    .separator {
        border: 2px solid rgba(0, 0, 0, .1);
    }

    .kotak {
        border: 1px;
    }
</style>

<img src="<?= base_url('publik/logo/infomedia.png') ?>" class="logo">
<div class="head">
    <h2>LIST REKOMENDASI <?= strtoupper($rowdata[0]->nama_kategori); ?></h2>
</div>
<hr>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="card-body">
            <table>
                <tr>
                    <td>Team Leader</td>
                    <td>:</td>
                    <td><?= $rowdata[0]->tl; ?></td>
                </tr>
            </table>
            <hr class="separator">
            <table border="1" class="table">
                <tr>
                    <th>NO</th>
                    <th>AGENT</th>
                    <th>JENIS</th>
                    <th>PERMASALAHAN</th>
                    <th>PENYULUHAN</th>
                    <th>REC BINA</th>
                </tr>
                <?php $o = 1;
                foreach ($rowdata as $row) : ?>
                    <tr>
                        <td style="text-align:left;"><?= $o++; ?></td>
                        <td style="text-align:left;"><?= $row->agent; ?></td>
                        <td style="text-align:left;"><?= $row->nama_subkts; ?></td>
                        <td style="text-align:left;"><?= $row->masalah; ?></td>
                        <td style="text-align:left;"><?= $row->penyuluhan; ?></td>
                        <td style="text-align:center;"><?= $row->tingkat; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
<hr class="separator">