
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Pemutihan extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email']) && $_SESSION['role'] = '23' || $_SESSION['role'] == '21') {
            redirect('login');
        }
    }

    public function index()
    {
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'pemutihan';
        $data['fullname'] = $_SESSION['fullname'];
        $this->load->view('default', $data);
    }

    public function loadHalaman($kategori)
    {
        $kategori = strtolower($this->uri->segment(2));
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'pemutihan';
        $data['fullname'] = $_SESSION['fullname'];
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vbina WHERE status='VERIFIKASI' AND LOWER(REPLACE(nama_kategori, ' ', ''))='$kategori'", 3);
        $data['kolom'] = array("Kode", "Agent", "TL", "SPV", "Masalah", "Kejadian", "Kategori", "Rec Bina", "Status", "Opsi");
        $this->load->view('default', $data);
    }

    function view_detail()
    {
        $id = $this->uri->segment(3);
        $kondisi = "kode";
        $data['rowdata'] = $this->Data_model->satuData('vbina', array($kondisi => $id));
        $this->load->view('pemutihan/view_detail', $data);
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $this->Data_model->jalankanQuery("UPDATE t_bina set status='SELESAI' WHERE kode=". $param);
                $this->Data_model->jalankanQuery("INSERT t_history_bina SELECT * FROM t_bina WHERE kode=". $param);
                // $this->Data_model->hapusDataWhere('t_history_bina', $kondisi);
                // echo json_encode("ok");
            }
        }
    }
}
