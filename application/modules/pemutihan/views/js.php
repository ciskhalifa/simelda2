<!-- Latest compiled and minified JavaScript -->
<script src="<?= base_url() ?>assets/admin/js/formValidation.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/validator.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.isloading.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jszip.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/pdfmake.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/vfs_fonts.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/select2/select2.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/dropzone/dropzone.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>

<div id="myConfirm" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan melakukan pemutihan pada <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-danger">Putihkan</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $("#PembinaanTable").dataTable();
        $(".detail").on('click', function() {
            $('#tabelpegawai').hide();
            $('#contentdetail').load('' + 'view_detail/' + $(this).attr("data-kode"));
            $('#containerdetail').fadeIn('fast');
        });

        $('#PembinaanTable tbody').on("click", ".putihkan", function() {
            $("#myConfirm").modal();
            $(".lblModal").text($(this).data('agent'));
            $("#cid").val($(this).data('kode'));
            $("#cod").val($(this).data('kat'));
            $("#getto").val("<?= base_url('pemutihan/hapus/');?>" + $(this).data('kode'));
        });

        $("#btnYes").bind("click", function() {
            var link = $("#getto").val();
            $.ajax({
                url: link,
                type: "POST",
                data: "cid=" + $("#cid").val() + "&cod=" + $("#cod").val(),
                dataType: "json",
                statusCode:{
                    500: function(){
                        notify('Duplicate entry!', 'danger');
                        setTimeout(function(){ location.reload(true); }, 1000);
                    }
                },
                beforeSend: function() {
                    if (link != "#") {}
                },
                    // location.reload(true);
                success: function() {
                    $("#myConfirm").modal("hide");
                    notify('Data berhasil diputihkan!', 'success');
                    setTimeout(function(){ location.reload(true); }, 2000);
                    
                }
            })
        });
    });
</script>