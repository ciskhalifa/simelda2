<div>
    <a href="javascript:;" id="back" class="btn btn-primary btn-md">
        <span class="m-0 font-weight-bold text-default text-center">BACK</span>
    </a>
    <?php if ($this->uri->segment(2) == 'pembinaan') : ?>
        <a href="<?= base_url('print/' . $rowdata->kode_jenis . '/' . $rowdata->kode) ?>" id="print" target="_BLANK" class="btn btn-success btn-md" data-jenis="<?= $rowdata->nama_jenis; ?>" data-kode="<?= $rowdata->kode; ?>">
            <span class="m-0 font-weight-bold text-default text-center"><i class="fa fa-print"></i></span>
        </a>
    <?php endif; ?>
</div>
<br>
<div class="row">
    <div class="col-md-6">
        <div class="card shadow mb-2 animated--grow-in">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary text-center">Profile</h6>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-4 label-control">Tanggal Pembinaan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="Tanggal Data" name="tgl_data" id="tgl_data" value="<?= date('d F Y', strtotime($rowdata->tgl_data)); ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Agent</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" value="<?= $rowdata->agent; ?>" data-error="wajib diisi" readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Team Leader</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" value="<?= $rowdata->tl; ?>" data-error="wajib diisi" readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Permasalahan</label>
                    <div class="col-md-8">
                        <textarea rows="5" class="form-control input-lg" placeholder="Permasalahan" name="alamat" id="alamat" readonly><?= $rowdata->masalah; ?></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Penyuluhan</label>
                    <div class="col-md-8">
                        <textarea rows="5" class="form-control input-lg" placeholder="Penyuluhan" name="alamat" id="alamat" readonly><?= $rowdata->penyuluhan; ?></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Status</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="status" name="status" id="status" value="<?= $rowdata->status; ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-2 animated--grow-in">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary text-center">Action Plan</h6>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-4 label-control">Tanggal Kejadian</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="Tanggal Kejadian" name="tgl_data" id="tgl_data" value="<?= date('d F Y', strtotime($rowdata->tgl_kejadian)); ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Batas Verifikasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="Tanggal Verifikasi" name="tgl_data" id="tgl_data" value="<?= date('d F Y', strtotime($rowdata->tgl_verifikasi)); ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Action Plan</label>
                    <div class="col-md-8">
                        <textarea class="form-control input-lg" placeholder="Action Plan" id="alamat" readonly><?= $rowdata->action_plan; ?></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Hasil Verifikasi</label>
                    <div class="col-md-8">
                        <textarea class="form-control input-lg" placeholder="Hasil Verifikasi" id="alamat" readonly><?= $rowdata->hasil_verifikasi; ?></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">TTD TL</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-lg" placeholder="Approval TL" id="alamat" value="<?= (!is_null($rowdata->approval_atasan)) ? date('d F Y', strtotime($rowdata->approval_atasan)) : 'Not Accept'; ?>" readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">TTD SPV</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-lg" placeholder="Approval SPV" id="alamat" value="<?= (!is_null($rowdata->approval_spv)) ? date('d F Y', strtotime($rowdata->approval_spv)) : 'Not Accept'; ?>" readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">TTD Agent</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-lg" placeholder="TTD Agent" id="alamat" value="<?= (!is_null($rowdata->approval_agent)) ? date('d F Y', strtotime($rowdata->approval_agent)) : 'Not Accept'; ?>" readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#back").on("click", function() {
        $("#tabelpegawai").fadeIn('fast');
        $("#containerform").fadeOut();
        $("#contentdetail").html("");
    });
    $("#approve").bind('click', function() {
        var link = "<?= base_url('spv/pembinaan/ApproveBina') ?>";
        $.ajax({
            url: link,
            type: "POST",
            data: "approval_spv=" + $(".approval_spv").find(':selected').val() + "&kode=" + $(this).data('kode'),
            dataType: "html",
            success: function(html) {
                notify("Update berhasil", "success");
                location.reload(true);
            }
        })
    })
    $("#print").bind('click', function() {
        var jenis = $(this).data('jenis');
        var kode = $(this).data('kode');
        var link = "<?= base_url('download') ?>";
        $.ajax({
            url: link,
            type: "POST",
            data: "nama_jenis=" + jenis + "&kode=" + kode,
            dataType: "html",
            success: function(html) {

            }
        })
    })
</script>