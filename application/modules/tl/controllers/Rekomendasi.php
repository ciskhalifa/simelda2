
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Rekomendasi extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email']) || $_SESSION['role'] !== '3') {
            redirect('login');
        }
        date_default_timezone_set("Asia/Bangkok");
    }

    public function index()
    {
        $data['js'] = 'pembinaan/js';
        $data['css'] = 'pembinaan/css';
        $data['content'] = 'pembinaan/rekomendasi';
        $data['kolom'] = array("Kode", "Agent", "TL", "SPV", "Masalah", "Kejadian", "Kategori", "Rec Bina", "Status", "Opsi");
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vrekomendasi WHERE vrekomendasi.status IS NULL AND view=" . $_SESSION['kode'], 3);
        $this->load->view('default', $data);
    }

    function view_detail()
    {
        $id = $this->uri->segment(4);
        $kondisi = "kode";
        $data['rowdata'] = $this->Data_model->satuData('vrekomendasi', array($kondisi => $id));
        $this->load->view('pembinaan/view_detail', $data);
    }

    public function updateBinaRekomendasi()
    {
        if (IS_AJAX) {
            $q = $this->Data_model->jalankanQuery("SELECT * FROM vrekomendasi WHERE kode=" . $this->input->post('kode'), 3);
            foreach ($q as $row) {
                $arrdata['kode_agent'] = $row->kode_agent;
                $arrdata['kode_tl'] = $row->kode_tl;
                $arrdata['kode_spv'] = $row->kode_spv;
                $arrdata['kode_subkts'] = $row->kode_sub;
                $arrdata['tgl_kejadian'] = $row->tgl_kejadian;
                $arrdata['masalah'] = $row->masalah;
                $arrdata['penyuluhan'] = $row->penyuluhan;
                $arrdata['evidence'] = $row->evidence;
                $arrdata['tgl_verifikasi'] = $row->tgl_verifikasi;
                $arrdata['approval_spv'] = $row->approval_spv;
            }
            $arrdata['tingkatan'] = $this->input->post('tingkatan');
            $arrdata['view'] = $_SESSION['kode'];
            $arrdata['approval_atasan'] = date('Y-m-d H:i:s');
            $arrdata['status'] = "VERIFIKASI";
            $arrdata2['status'] = $this->input->post('status');
            $this->Data_model->simpanData($arrdata, 't_bina');
            $this->Data_model->updateDataWhere($arrdata2, 't_rekomendasi', array('kode' => $this->input->post('kode')));
            $kode = $this->Data_model->getLastIdDb('t_bina', 'kode');
            $email = "mbenakal10@gmail.com";
            modules::load('utils/SendEmailController')->sendEmail($email, $kode, 'Bina');
            echo json_encode("ok");
        }
    }
}
