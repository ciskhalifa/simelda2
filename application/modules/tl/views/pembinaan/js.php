<!-- Latest compiled and minified JavaScript -->
<script src="<?= base_url() ?>assets/admin/js/formValidation.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/validator.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.isloading.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jszip.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/pdfmake.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/vfs_fonts.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/select2/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>

<div id="myUpdate" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Hasil Verifikasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-4 label-control">Status Pembinaan</label>
                        <div class="col-md-8">
                            <select class="select2 form-control status" name="status" id="status" style="width:100%">
                                <option value="">- Pilihan -</option>
                                <option value="VERIFIKASI">VERIFIKASI</option>
                                <option value="SELESAI">SELESAI</option>
                            </select>
                        </div>
                    </div>
                    <div class="hasil_verifikasi" style="display:none;">
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Hasil Verifikasi</label>
                            <div class="col-md-8">
                                <textarea class="form-control" placeholder="Hasil Verifikasi" name="hasil_verifikasi"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" id="kode">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnUpdate" class="btn btn-warning">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myUpdateRekomendasi" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Buat Pembinaan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-4 label-control">Jenis Pembinaan</label>
                        <div class="col-md-8">
                            <select class="select2 form-control jenis_bina" name="" id="jenis_bina" style="width:100%" require>
                                <option value="">- Pilihan -</option>
                                <?php
                                $q = $this->Data_model->selectData('m_jenis', 'kode');
                                foreach ($q as $row) {
                                    echo '<option data-id="' . $row->kode . '" value="' . $row->kode . '">' . $row->nama_jenis . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="select_tingkat" style="display:none;">
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Tingkatan</label>
                            <div class="col-md-8">
                                <select class="select2 form-control tingkatan" name="tingkatan" id="tingkatan" style="width:100%" require>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 label-control">Status Pembinaan</label>
                        <div class="col-md-8">
                            <select class="select2 form-control status" name="status2" id="status2" style="width:100%">
                                <option value="PROSES">PROSES</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="kode">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnUpdateRekomendasi" class="btn btn-warning">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(function() {
        $("#PembinaanTable").dataTable();
        $('#openform').on('click', function() {
            $('#modalForm').modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        $('#modalForm').on('hidden.bs.modal', function() {
            $(this).find('form')[0].reset();
            if ($("#pesanan tbody tr").length > 1) {
                $("#pesanan tbody tr:last-child").remove();
            }
        });
        $(".jenis_bina").on('change', function() {
            var id = $(this).find(':selected').attr('data-id');
            if (id == '1' || id == '2' || id == '3' || id == '4') {
                $('.select_tingkat').css('display', '');
            } else {
                $('.select_tingkat').css('display', 'none');
            }
            var link = "<?= base_url('tl/pembinaan/getTingkatan') ?>";
            $.ajax({
                url: link,
                type: "POST",
                data: "kode_jenis=" + id,
                dataType: "html",
                success: function(html) {
                    json = eval(html);
                    $(".tingkatan").empty();
                    $(json).each(function() {
                        $(".tingkatan").append('<option value="' + this.kode + '">' + this.disp + "</option>")
                    });
                }
            })
        });
        $(".kode_subkts").on('change', function() {
            var id = $(this).find(':selected').attr('data-id');
            var link = "<?= base_url('tl/pembinaan/getVerifikasi') ?>";
            $.ajax({
                url: link,
                type: "POST",
                data: "kode=" + id,
                dataType: "json",
                success: function(html) {
                    $('.waktu').val(html[0].jumlah_hari);
                }
            })
        });
        var now = moment().format('YYYY-MM-DD');
        $('.tgl_bina').val(now);
        $('.tgl_kejadian').on('change', function() {
            moment.locale('id');
            var c = moment().format('YYYY-MM-DD');
            var a = moment(c, 'YYYY-MM-DD').add($('.waktu').val(), 'days').format('YYYY-MM-DD');
            $(".tgl_verifikasi").empty().val(a);
        });

        $(".detail").on('click', function() {
            $('#tabelpegawai').hide();
            $('#contentdetail').load('' + 'pembinaan/view_detail/' + $(this).attr("data-kode"));
            $('#containerdetail').fadeIn('fast');
        });

        $(".detailrekomendasi").on('click', function() {
            $('#tabelpegawai').hide();
            $('#contentdetail').load('' + 'rekomendasi/view_detail/' + $(this).attr("data-kode"));
            $('#containerdetail').fadeIn('fast');
        });

        $(".edit").on('click', function() {
            if ($(this).data('status') != 'SELESAI') {
                $('#myUpdate').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $("#kode").val($(this).data('kode'));
            }
        });

        $(".editrekomendasi").on('click', function() {
            $('#myUpdateRekomendasi').modal({
                backdrop: 'static',
                keyboard: false
            });
            $("#kode").val($(this).data('kode'));
            $(".tingkat").val($(this).data('tingkat'));
        })

        $(".status").on('change', function() {
            var data = $(this).find(":selected").val();
            if (data == "SELESAI") {
                $('.hasil_verifikasi').css('display', '');
            } else {
                $('.hasil_verifikasi').css('display', 'none');
            }
        });

        $("#btnUpdate").bind('click', function() {
            var link = "<?= base_url('tl/pembinaan/updateBina') ?>";
            $.ajax({
                url: link,
                type: "POST",
                data: "kode=" + $("#kode").val() + "&status=" + $("#status").val() + "&hasil_verifikasi=" + $('.hasil_verifikasi').val(),
                dataType: "html",
                success: function(html) {
                    notify("Update berhasil", "success")
                    $("#updateBina").modal("hide");
                    location.reload(true);
                }
            })
        });

        $("#btnUpdateRekomendasi").bind('click', function() {
            var link = "<?= base_url('tl/rekomendasi/updateBinaRekomendasi') ?>";
            $.ajax({
                url: link,
                type: "POST",
                data: "kode=" + $("#kode").val() + "&status=" + $('#status2').find(":selected").val() + "&jenis_bina=" + $('.jenis_bina').val() + "&tingkatan=" + $('.tingkatan').val(),
                dataType: "html",
                success: function(html) {
                    notify("Simpan berhasil", "success")
                    $("#updateBinaRekomendasi").modal("hide");
                    location.reload(true);
                }
            })
        });

    });
</script>