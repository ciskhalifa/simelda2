<link href="<?= base_url('assets/admin') ?>/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="<?= base_url('assets/admin/css/buttons.bootstrap4.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('assets/admin') ?>/vendor/dropzone/dropzone.css" rel="stylesheet">

<style>
    .box-tools {
        position: absolute;
        right: 10px;
        top: 5px;
    }

    .col-label {
        position: relative;
        width: 100%;
        padding-right: .75rem;
    }

    div.col-label.col-md-5 {
        position: relative;
        width: 100%;
        padding-right: .75rem;
    }

    .rounded-circle-b {
        border-radius: 10% !important
    }

    .col-label-b {
        position: relative;
        width: 100%;
        padding-right: .75rem;
        font-weight: bold;
    }

    #back {
        width: 100px;
        border-radius: 4px;
        text-align: center;
        transition: all 0.5s;
        cursor: pointer;
    }

    #back span {
        cursor: pointer;
        display: inline-block;
        position: relative;
        transition: 0.5s;
    }

    #back span:after {
        content: '\00ab';
        position: absolute;
        opacity: 1;
        top: -13px;
        left: -70px;
        transition: 0.5s;
        font-size: 31px;
    }

    #back:hover span {
        padding-left: 20px;
    }

    #back:hover span:after {
        opacity: 1;
        right: 0;
    }

    .btn-info-b {
        color: #fff;
        background-color: #4e73df !important;
        border-color: #4e73df !important;
    }
</style>