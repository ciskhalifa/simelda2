
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Pembinaan extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email']) || $_SESSION['role'] !== '23') {
            redirect('login');
        }
    }

    public function index()
    {
        $data['js'] = 'pembinaan/js';
        $data['css'] = 'pembinaan/css';
        $data['content'] = 'pembinaan/pembinaan';
        $data['kolom'] = array("Kode", "TL", "Agent", "Jenis Bina", "Masalah", "Kejadian", "Bina", "Batas Verifikasi", "Status", "Opsi");
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vbina", 3);
        $this->load->view('default', $data);
    }

    public function updateBina()
    {
        if (IS_AJAX) {
            $arrdata['status'] = $this->input->post('status');
            $arrdata['hasil_verifikasi'] = $this->input->post('hasil_verifikasi');
            $this->Data_model->updateDataWhere($arrdata, 't_bina', array('kode' => $this->input->post('kode')));
            echo json_encode("ok");
        }
    }

    public function simpanData()
    {

        $arrdata = array();
        $cid = '';

        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) { } else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                switch (trim($key)):
                    case 'cid':
                        $cid = $value;
                        break;
                    default:
                        if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                            if (strlen(trim($value)) > 0) {
                                $tgl = explode("-", $value);
                                $newtgl = $tgl[1] . "-" . $tgl[0] . "-" . $tgl[2];
                                $time = strtotime($newtgl);
                                $arrdata[$key] = $value; // date('Y-m-d', $time);
                            } else {
                                $arrdata[$key] = null;
                            }
                        } else {
                            $arrdata[$key] = $value;
                        }
                        break;
                endswitch;
            }
        }

        if ($this->input->post('kode') == "") {
            $this->Data_model->simpanData($arrdata, 't_bina');
        } else {
            $kondisi = 'kode';
            $this->Data_model->updateDataWhere($arrdata, 't_bina', array($kondisi => $this->input->post('kode')));
        }
        redirect('qc/pembinaan');
    }

    public function getAgent()
    {
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vcoaching WHERE kode=" . $this->input->post('kode'), 3);
        echo json_encode($q);
    }
    public function getTingkatan()
    {
        $jenis = $this->input->post('kode_jenis');
        if ($jenis == "") {
            $q = $this->Data_model->jalankanQuery("SELECT kode, tingkat as disp FROM m_tingkat WHERE 1=1", 3);
        } else {
            $q = $this->Data_model->jalankanQuery("SELECT kode, tingkat as disp FROM m_tingkat WHERE kode_jenis=" . $jenis, 3);
        }
        echo json_encode($q);
    }
    public function getVerifikasi()
    {
        $kode = $this->input->post('kode');
        $tgl = $this->input->post('tgl_kejadian');
        $q = $this->Data_model->jalankanQuery("SELECT m_subkts.*, m_verifikasi.jumlah_hari FROM m_subkts JOIN m_verifikasi ON m_verifikasi.kode = m_subkts.kode_verifikasi WHERE m_subkts.kode=" . $kode, 3);
        echo json_encode($q);
    }
    public function getDataBina()
    {
        $kode = $this->input->post('kode');
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vbina WHERE kode=" . $kode, 3);
        echo json_encode($q);
    }
    public function view_detail()
    {
        $id = $this->uri->segment(4);
        $kondisi = "kode";
        $data['rowdata'] = $this->Data_model->satuData('vbina', array($kondisi => $id));
        $this->load->view('pembinaan/view_detail', $data);
    }

}
