<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Login extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'login';

        $this->load->view('layout_login', $data);
    }

    public function doLogin()
    {
        if (IS_AJAX) {
            $password = $this->input->post('password');
            $user = $this->db->get_where('m_users', ['email' => $this->input->post('email')])->row_array();
            if ($user) {
                $res = $this->Data_model->verify_user(array('email' => $this->input->post('email'), 'password' => password_verify($password, $user['password'])), 'm_users');
                if ($res->is_active == 1) {
                    if ($res !== FALSE) {
                        foreach ($res as $row => $kolom) {
                            $_SESSION[$row] = $kolom;
                        }
                        switch ($res->role) {
                            case 1:
                                echo base_url('dashboard');
                                break;
                            case 21:
                                echo base_url('spv/dashboard');
                                break;
                            case 22:
                                echo base_url('admin/dashboard');
                                break;
                            case 23:
                                echo base_url('qc/dashboard');
                                break;
                            case 3:
                                echo base_url('tl/dashboard');
                                break;
                            case 4:
                                echo base_url('agent/dashboard');
                                break;
                            default:
                                break;
                        }
                    } else {
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">*</span></button>Your Email & Password Combination does not match!';
                    }
                } else {
                    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">*</span></button>Your Account not Active!';
                }
            } else {
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">*</span></button>This email is not registered!';
            }
        }
    }

    public function doOut()
    {
        session_unset();
        session_destroy();
        redirect('login');
    }
}
