<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Register extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'register';

        $this->load->view('layout_login', $data);
    }

    public function doRegister()
    {
        if (IS_AJAX) {
            $this->form_validation->set_rules('fullname', 'Full Name', 'required|trim');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[m_users.email]', ['is_unique' => 'This email has been registered!']);
            $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', ['matches' => 'Password does not match', 'min_length' => 'Password too short']);
            $this->form_validation->set_rules('password2', 'Repeat Password', 'required|trim|matches[password1]', [
                'matches' => 'Repeat Password does not match',
            ]);
            if ($this->form_validation->run() == false) {
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">*</span></button> Password does not match.';
            }
            $users = array(
                'fullname' => $this->input->post('fullname'),
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password1'),
                'role_id' => 2,
                'is_active' => 0
            );
            $res2 = $this->Data_model->updateData($users, 'm_users');
            if ($res2 !== FALSE) {
                echo base_url('login');
            } else {
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">*</span></button> Password / Username tidak ditemukan/salah.';
            }
        }
    }
    
    public function verification($id){
        
    }
}
