<!-- Latest compiled and minified JavaScript -->
<script src="<?= base_url() ?>assets/admin/js/formValidation.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/validator.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.isloading.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jszip.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/pdfmake.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/vfs_fonts.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/select2/select2.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/dropzone/dropzone.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>
<div id="myConfirm" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan melakukan <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-danger">Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myUpdate" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Hasil Verifikasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-4 label-control">Status Pembinaan</label>
                        <div class="col-md-8">
                            <select class="select2 form-control status" name="status" id="status" style="width:100%">
                                <option value="">- Pilihan -</option>
                                <option value="VERIFIKASI">VERIFIKASI</option>
                                <option value="SELESAI">SELESAI</option>
                            </select>
                        </div>
                    </div>
                    <div class="hasil_verifikasi" style="display:none;">
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Hasil Verifikasi</label>
                            <div class="col-md-8">
                                <textarea class="form-control" placeholder="Hasil Verifikasi" name="hasil_verifikasi"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="kode">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnUpdate" class="btn btn-warning">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {

        $("#PembinaanTable").dataTable();
        $(".jenis_bina").on('change', function() {
            var id = $(this).find(':selected').attr('data-id');
            if (id == '1' || id == '2' || id == '3' || id == '4') {
                $('.select_tingkat').css('display', '');
            } else {
                $('.select_tingkat').css('display', 'none');
            }
            var link = "<?= base_url('admin/pembinaan/getTingkatan') ?>";
            $.ajax({
                url: link,
                type: "POST",
                data: "kode_jenis=" + id,
                dataType: "html",
                success: function(html) {
                    json = eval(html);
                    $(".tingkatan").empty();
                    $(json).each(function() {
                        $(".tingkatan").append('<option value="' + this.kode + '">' + this.disp + "</option>")
                    });
                }
            })
        });
        $(".kode_subkts").on('change', function() {
            var id = $(this).find(':selected').attr('data-id');
            var link = "<?= base_url('admin/pembinaan/getVerifikasi') ?>";
            $.ajax({
                url: link,
                type: "POST",
                data: "kode=" + id,
                dataType: "json",
                success: function(html) {
                    $('.waktu').val(html[0].jumlah_hari);
                }
            })
        });

        $('.tgl_kejadian').on('change', function() {
            moment.locale('id');
            var a = moment($(this).val(), 'YYYY-MM-DD').add($('.waktu').val(), 'days').format('YYYY-MM-DD');
            $(".tgl_verifikasi").empty().val(a);
        });

        $(".detail").on('click', function() {
            $('#tabelpegawai').hide();
            $('#contentdetail').load('' + 'pembinaan/view_detail/' + $(this).attr("data-kode"));
            $('#containerdetail').fadeIn('fast');
        });

        $(".detailrekomendasi").on('click', function() {
            $('#tabelpegawai').hide();
            $('#contentdetail').load('' + 'rekomendasi/view_detail/' + $(this).attr("data-kode"));
            $('#containerdetail').fadeIn('fast');
        });

        $(".edit").on('click', function() {
            if ($(this).data('status') != 'SELESAI') {
                $.ajax({
                    url: "<?= base_url('admin/pembinaan/getTingkatan') ?>",
                    type: "POST",
                    data: "kode_jenis=",
                    dataType: "html",
                    success: function(html) {
                        json = eval(html);
                        $(".tingkatan").empty();
                        $(json).each(function() {
                            $(".tingkatan").append('<option value="' + this.kode + '">' + this.disp + "</option>")
                        });
                    }
                })
                var link = "<?= base_url('admin/pembinaan/getDataBina') ?>";
                $.ajax({
                    url: link,
                    type: "POST",
                    data: "kode=" + $(this).data('kode'),
                    dataType: "json",
                    success: function(html) {
                        $("#kode").val(html[0].kode);
                        $(".agent").find("option[value=" + html[0].kode_agent + "]").prop('selected', true);
                        $(".tgl_bina").val(html[0].tgl_bina);
                        $(".tgl_kejadian").val(html[0].tgl_kejadian);
                        $(".tingkatan").find("option[value=" + html[0].kode_tingkat + "]").prop("selected", true);
                        $(".kode_subkts").find("option[value=" + html[0].kode_sub + "]").prop('selected', true);
                        $(".jenis_bina").find("option[value=" + html[0].kode_jenis + "]").prop('selected', true);
                        $(".tgl_verifikasi").val(html[0].tgl_verifikasi);
                        $(".masalah").text(html[0].masalah);
                        $(".penyuluhan").text(html[0].penyuluhan);
                    }
                })
                $('#modalForm').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $("#kode").val($(this).data('kode'));

            }
        });

        $(".status").on('change', function() {
            var data = $(this).find(":selected").val();
            if (data == "SELESAI") {
                $('.hasil_verifikasi').css('display', '');
            } else {
                $('.hasil_verifikasi').css('display', 'none');
            }
        });

        $("#btnUpdate").bind('click', function() {
            var link = "<?= base_url('admin/pembinaan/updateBina') ?>";
            $.ajax({
                url: link,
                type: "POST",
                data: "kode=" + $("#kode").val() + "&status=" + $("#status").val() + "&hasil_verifikasi=" + $('.hasil_verifikasi').val(),
                dataType: "html",
                success: function(html) {
                    notify("Update berhasil", "success")
                    $("#updateBina").modal("hide");
                    location.reload(true);
                }
            })
        });

        $('#import').on('click', function() {
            $('#modalImport').modal();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#file").on("change", function() {
            var file = this.files[0];
            var fileName = file.name;
            var fileType = file.type;
            console.log(fileType);
            if (fileType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || fileType == "application/vnd.ms-excel") {
                $('#label').text(fileName);
                $('#reset').removeClass('hidden');
            } else {
                alert("Maaf format dokumen tidak sesuai");
                $(this).val('');
                $('#label').text('Pilih File');
            }
        });

        $('#sendForm').validator().on('submit', function(e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            } else {
                var link = 'rekomendasi/importExcel/';
                var data = new FormData(this);
                $.ajax({
                    sync: true,
                    url: link,
                    data: data,
                    type: 'POST',
                    datatype: 'html',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $("body").isLoading({
                            text: "",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%" style="background:none;">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"/></svg></div>'
                        });
                    },
                    success: function(html) {
                        setTimeout(function() {
                            scrollTo();
                            $('#modalImport').modal('hide');
                            notify('Data berhasil disimpan!', 'success');
                        }, 500);
                        $("body").isLoading("hide");
                        // location.reload(true);

                    },
                    error: function() {
                        notify('Data gagal disimpan!', 'warning');
                        $('#modalImport').modal('hide');
                        $("body").isLoading("hide");
                        // location.reload(true);

                    },
                });
                return false;
            }
        });

        $(".deleterekomendasi").on('click', function() {
            $("#myConfirm").modal();
            $(".lblModal").text($(this).data('kode'));
            $("#cid").val($(this).data('kode'));
            $("#getto").val("<?= base_url('admin/rekomendasi/hapus') ?>");
        });

        $("#btnYes").bind("click", function() {
            var link = $("#getto").val();
            $.ajax({
                url: link,
                type: "POST",
                data: "cid=" + $("#cid").val() + "&cod=" + $("#tabel").val(),
                dataType: "html",
                beforeSend: function() {
                    if (link != "#") {}
                },
                success: function(html) {
                    $("#myConfirm").modal("hide");
                    notify('Data berhasil dihapus!', 'success');
                    location.reload(true);
                }
            })
        });
    });
</script>