<div class="container-fluid">
    <div id="tabelpegawai" class="slideInDown animated--grow-in" data-appear="appear" data-animation="slideInDown">
        <div class="content-detached">
            <div class="content-body">
                <section class="row">
                    <div class="col-md-12">
                        <div class="card shadow mb-2">
                            <div class="card-header py-3">
                                <h4 class="m-0 font-weight-bold text-primary"> List <?= $this->uri->segment('2'); ?></h4>
                                <a class="heading-elements-toggle"><i class="icon-arrow-right-4"></i></a>
                            </div>
                            <div class="card-body">
                                <table id="PembinaanTable" class="table table-white-space table-bordered ">
                                    <thead>
                                        <tr>
                                            <?php
                                            if ($kolom) {
                                                foreach ($kolom as $key => $value) {
                                                    if (strlen($value) == 0) {
                                                        echo '<th data-type="numeric"></th>';
                                                    } else {
                                                        echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rowdata as $row) : ?>
                                            <tr>
                                                <td><?= $row->kode; ?></td>
                                                <td><?= $row->tl; ?></td>
                                                <td><?= $row->agent; ?></td>
                                                <td><?= $row->nama_jenis; ?></td>
                                                <td><?= $row->masalah; ?></td>
                                                <td><?= date('d F Y', strtotime($row->tgl_kejadian)); ?></td>
                                                <td><?= date('d F Y', strtotime($row->tgl_data)); ?></td>
                                                <td><?= date('d F Y', strtotime($row->tgl_verifikasi)); ?></td>
                                                <td><?= $row->status; ?></td>
                                                <td style="width:150px;">
                                                    <button type="button" data-status="<?= $row->status ?>" data-kode="<?= $row->kode ?>" class="btn btn-sm btn-warning edit" id="edit"><i class="fa fa-edit"></i></button>
                                                    <button type="button" data-status="<?= $row->status ?>" data-kode="<?= $row->kode ?>" class="btn btn-sm btn-info detail" id="detail"><i class="fa fa-eye"></i></button>
                                                    <button type="button" data-status="<?= $row->status ?>" data-kode="<?= $row->kode ?>" class="btn btn-sm btn-danger delete" id="delete"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div id="containerdetail" class="content-detached">
        <div class="content-body">
            <div id="contentdetail">
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-xs-left animated--grow-in" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form role="form" id="sendForm" enctype="multipart/form-data" class="form-horizontal" action="<?= base_url('admin/pembinaan/simpanData'); ?>" method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Form Edit Pembinaan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" name="kode" id="kode">
                        <label class="col-md-4 label-control">Agent</label>
                        <div class="col-md-8">
                            <select class="select2 form-control agent" name="kode_agent" id="agent" style="width:100%">
                                <option value="">- Pilihan -</option>
                                <?php
                                $n = (isset($arey)) ? $arey['kode_agent'] : '';
                                $q = $this->Data_model->selectData('m_users', 'kode', array('role' => 4));
                                foreach ($q as $row) {
                                    $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                                    echo '<option data-id="' . $row->kode . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->fullname . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 label-control">Jenis Pembinaan</label>
                        <div class="col-md-8">
                            <select class="select2 form-control jenis_bina" name="" id="jenis_bina" style="width:100%" require>
                                <option value="">- Pilihan -</option>
                                <?php
                                $q = $this->Data_model->selectData('m_jenis', 'kode');
                                foreach ($q as $row) {
                                    echo '<option data-id="' . $row->kode . '" value="' . $row->kode . '">' . $row->nama_jenis . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="select_tingkat" style="">
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Tingkatan</label>
                            <div class="col-md-8">
                                <select class="select2 form-control tingkatan" name="tingkatan" id="tingkatan" style="width:100%" require>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 label-control">Ketidaksesuaian</label>
                        <div class="col-md-8">
                            <select class="select2 form-control kode_subkts" name="kode_subkts" id="kode_subkts" style="width:100%" require>
                                <option value="">- Pilihan -</option>
                                <?php
                                $q = $this->Data_model->selectData('m_subkts', 'kode');
                                foreach ($q as $row) {
                                    echo '<option data-id="' . $row->kode . '" value="' . $row->kode . '">' . $row->nama_subkts . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" class="waktu">
                    <div class="form-group row">
                        <label class="col-md-4 label-control">Tanggal Kejadian</label>
                        <div class="col-md-4">
                            <input type="date" data-date="" data-date-format="YYYY-MM-DD" class="form-control tgl_kejadian" name="tgl_kejadian" id="tgl_kejadian" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 label-control">Tanggal Verifikasi</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control tgl_verifikasi" name="tgl_verifikasi" id="tgl_verifikasi" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 label-control">Permasalahan</label>
                        <div class="col-md-8">
                            <textarea class="form-control masalah" placeholder="Permasalahan" name="masalah" id="masalah" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 label-control">Penyuluhan</label>
                        <div class="col-md-8">
                            <textarea class="form-control penyuluhan" placeholder="Penyuluhan" name="penyuluhan" id="penyuluhan" required></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>