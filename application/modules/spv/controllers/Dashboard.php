
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Dashboard extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email']) && $_SESSION['role'] == '21') {
            redirect('login');
        }
    }

    public function index()
    {

        $data['js'] = 'dashboard/js';
        $data['css'] = 'dashboard/css';
        $data['content'] = 'dashboard/dashboard';
        $data['fullname'] = $_SESSION['fullname'];
        $this->load->view('default', $data);
    }
}
