
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Rekomendasi extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email']) || $_SESSION['role'] !== '21') {
            redirect('login');
        }
    }

    public function index()
    {
        $data['js'] = 'pembinaan/js';
        $data['css'] = 'pembinaan/css';
        $data['content'] = 'rekomendasi/rekomendasi';
        $data['kolom'] = array("Kode", "Agent", "TL", "SPV", "Masalah", "Kejadian", "Kategori", "Rec Bina", "Status", "Opsi");
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vrekomendasi WHERE kode_spv=" . $_SESSION['kode'], 3);
        $this->load->view('default', $data);
    }

    public function importExcel()
    {
        $basepath = BASEPATH;
        $stringreplace = str_replace("system", "publik/doc/", $basepath);
        $config['upload_path'] = $stringreplace;
        $config['allowed_types'] = 'xls|xlsx';
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        $files = $_FILES;
        $_FILES['userfile']['name'] = $files['file']['name'];
        $_FILES['userfile']['type'] = $files['file']['type'];
        $_FILES['userfile']['tmp_name'] = $files['file']['tmp_name'];
        $_FILES['userfile']['error'] = $files['file']['error'];
        $_FILES['userfile']['size'] = $files['file']['size'];
        $new_name = str_replace(" ", "_", $_FILES["file"]['name']);
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if ($this->upload->do_upload()) {
            $this->prosesXLS($new_name);
        } else {
            $error = array('error' => $this->upload->display_errors());
        }
    }

    function prosesXLS($namafile)
    {
        error_reporting(E_ALL & ~E_NOTICE);
        $urutankode = 1;
        $file = 'publik/doc/' . $namafile;
        /*
         * load the excel library
         */
        $this->load->library('excel');
        /*
         * read file from path
         */
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        /*
         * get only the Cell Collection
         */

        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        /*
         * extract to a PHP readable array format
         */

        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            $data_type = $objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue();

            /*
             * header will/should be in row 1 only. of course this can be modified to suit your need.
             */
            if ($row <= 6) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        }

        $kolom = array("kode_agent", "kode_tl", "kode_subkts", "tgl_kejadian", "masalah", "penyuluhan");

        foreach ($arr_data as $row) :
            if ($this->cekIsiData($row, $baris) == true) :
                $kode = intval($kl) . $urutankode;
                /*
                 * $x := nilai untuk array $kolom
                 * $k := nilai untuk array xls
                 */
                $x = 0;
                $k = 1;
                $data = array();
                foreach ($row as $col) :
                    if (substr($col, 0, 1) == "=") {
                        $nilai = "";
                    } else {
                        $nilai = ($col == '') ? NULL : $col;
                    }
                    switch ($x):
                        case 3:
                        case 12:
                            break;
                        case 2:
                            $pisah = explode(' ', $nilai);
                            $carikode = $this->Data_model->jalankanQuery("SELECT * FROM m_users WHERE fullname LIKE '%$pisah[0]%' LIMIT 1", 1);
                            $data['kode_agent'] = ($carikode) ? $carikode->kode : '';
                            break;
                        case 4:
                            $pisah = explode(' ', $nilai);
                            $carikode = $this->Data_model->jalankanQuery("SELECT * FROM m_users WHERE fullname LIKE '%$pisah[0]%' LIMIT 1", 1);
                            $data['kode_tl'] = ($carikode) ? $carikode->kode : '';
                            $data['view'] = ($carikode) ? $carikode->kode : '';
                            break;
                        case 5:
                            $pisah = explode(' ', $nilai);
                            $carikode = $this->Data_model->jalankanQuery("SELECT * FROM m_users WHERE fullname LIKE '%$pisah[0]%' LIMIT 1", 1);
                            $data['kode_spv'] = ($carikode) ? $carikode->kode : '';
                            break;
                        case 7:
                            $carikata =  str_replace(array("\r", "\n"), "", $nilai);
                            $hasil = preg_split('/(?:Rek)\W+/', $carikata);
                            $data['masalah'] = ($hasil) ? $hasil[0] : "";
                            $data['penyuluhan'] = ($hasil) ? $hasil[1] : "";
                            break;
                        case 8:
                            $UNIX_DATE = ($nilai - 25569) * 86400;
                            if ($UNIX_DATE < 0) {
                                $data['tgl_kejadian'] = $this->libglobal->dateReverse($nilai);
                            } else {
                                $data['tgl_kejadian'] = ($nilai == '') ? NULL : gmdate("Y-m-d", $UNIX_DATE);
                            }
                            break;
                        case 9:
                            $data['evidence'] = $nilai;
                            break;
                        case 10:
                            $kata = str_replace(array("\r", "\n", " "), "", $nilai);
                            $carikode = $this->Data_model->jalankanQuery("SELECT m_subkts.*, m_verifikasi.jumlah_hari FROM m_subkts JOIN m_verifikasi ON m_verifikasi.kode = m_subkts.kode_verifikasi WHERE REPLACE(nama_subkts, ' ', '') LIKE '%$kata%' LIMIT 1", 1);
                            $data['kode_subkts'] = ($carikode) ? $carikode->kode : '';
                            $data['tgl_verifikasi'] = date('Y-m-d', strtotime("+" . $carikode->jumlah_hari . " days", strtotime($data['tgl_kejadian'])));
                            break;
                        case 11:
                            $tingkat = strtoupper($nilai);
                            $carikode = $this->Data_model->jalankanQuery("SELECT * FROM m_tingkat WHERE tingkat = '$tingkat' LIMIT 1", 1);
                            $data['tingkatan'] = ($carikode) ? $carikode->kode : '';
                            break;
                        default:
                            // $data[$kolom[$x]] = $nilai;
                            break;
                    endswitch;
                    $k++;
                    $x++;

                endforeach;
                $i++;
                try {
                    // print_r($data);
                    // die;
                    $this->Data_model->simpanData($data, 't_rekomendasi');
                } catch (Exception $exc) {
                    $data['infoerror'] = $exc->getMessage();
                } else :

            endif;

        endforeach;
    }

    function cekIsiData($row, $baris)
    {
        $isi = false;
        foreach ($row as $eusi) {
            $isi = (strlen(trim($eusi)) > 0) ? true : (($isi == true) ? $isi : false);
        }
        return $isi;
    }
    function view_detail()
    {
        $id = $this->uri->segment(4);
        $kondisi = "kode";
        $data['rowdata'] = $this->Data_model->satuData('vrekomendasi', array($kondisi => $id));
        $this->load->view('pembinaan/view_detail', $data);
    }

    function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $this->Data_model->hapusDataWhere('t_rekomendasi', $kondisi);
                echo json_encode("ok");
            }
        }
    }
}
