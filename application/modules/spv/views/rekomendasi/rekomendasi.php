<div class="container-fluid">
    <div id="tabelpegawai" class="slideInDown animated--grow-in" data-appear="appear" data-animation="slideInDown">
        <div class="content-detached">
            <div class="content-body">
                <section class="row">
                    <div class="col-md-12">
                        <div class="card shadow mb-2">
                            <div class="card-header py-3">
                                <h4 class="m-0 font-weight-bold text-primary"> List <?= $this->uri->segment('2'); ?></h4>
                                <a class="heading-elements-toggle"><i class="icon-arrow-right-4"></i></a>
                                <div class="box-tools pull-right">
                                    <a href="javascript:;" class="btn btn-warning btn-sm" id="import" data-tab="" data-original-title="Import Data" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="">
                                        <i class="fa fa-upload"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="PembinaanTable" class="table table-white-space table-bordered ">
                                    <thead>
                                        <tr>
                                            <?php
                                            if ($kolom) {
                                                foreach ($kolom as $key => $value) {
                                                    if (strlen($value) == 0) {
                                                        echo '<th data-type="numeric"></th>';
                                                    } else {
                                                        echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rowdata as $row) : ?>
                                            <tr>
                                                <td><?= $row->kode; ?></td>
                                                <td><?= $row->agent; ?></td>
                                                <td><?= $row->tl; ?></td>
                                                <td><?= $row->spv; ?></td>
                                                <td style="word-wrap: break-word"><?= $row->masalah; ?></td>
                                                <td><?= date('d F Y', strtotime($row->tgl_kejadian)); ?></td>
                                                <td><?= $row->nama_kategori; ?></td>
                                                <td><?= $row->tingkat; ?></td>
                                                <td><?= $row->status; ?></td>
                                                <td style="width:100px;">
                                                    <button type="button" data-status="<?= $row->status ?>" data-kode="<?= $row->kode ?>" class="btn btn-sm btn-info detailrekomendasi" id="detailrekomendasi"><i class="fa fa-eye"></i></button>
                                                    <button type="button" data-status="<?= $row->status ?>" data-kode="<?= $row->kode ?>" class="btn btn-sm btn-danger deleterekomendasi" id="deleterekomendasi"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div id="containerdetail" class="content-detached">
        <div class="content-body">
            <div id="contentdetail">
            </div>
        </div>
    </div>

    <div class="modal fade text-xs-left animated--grow-in" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form role="form" id="sendForm" enctype="multipart/form-data" class="form-horizontal">
                    <div class="modal-header">
                        <h4 class="modal-title">Import Data Rekomendasi</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input id="file" name="file" type="file" style="" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Upload</button>
                        <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>