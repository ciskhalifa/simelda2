
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Roster extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email']) && $_SESSION['role'] == '4') {
            redirect('login');
        }
        date_default_timezone_set("Asia/Bangkok");

    }

    public function index()
    {
        $data['js'] = 'roster/js';
        $data['css'] = 'roster/css';
        $data['content'] = 'roster/roster';
        $data['kolom'] = array("Tanggal", "Hari", "Perner ID", "Shift", "Login", "lst 1", "lst 2", "lst 3", "Logout", "WS", "Keterangan");
        $data['form_action'] = base_url('agent/roster/view_data');
        $pos = $_SESSION['role'] == "4" ? $pos = "TA" : "";
        $data['list_sdm'] = $this->Data_model->load_data_sdm($pos);
        $data['list_jadwal_ta'] = array();
        $data['x_perner_id']    = $_SESSION['kode'];
        $this->load->view('default', $data);
    }

    public function view_data(){

        $data['js'] = 'roster/js';
        $data['css'] = 'roster/css';
        $data['content'] = 'roster/roster';
        $data['kolom'] = array("Tanggal", "Hari", "Perner ID", "Shift", "Login", "lst 1", "lst 2", "lst 3", "Logout", "WS", "Keterangan");
        $data['form_action'] = base_url('agent/roster/view_data');
        $pos = $_SESSION['role'] == "4" ? $pos = "TA" : "";
        $data['list_sdm'] = $this->Data_model->load_data_sdm($pos);
        $data['date_1']         = $this->input->post('date_1');
        $data['x_perner_id']    = $this->input->post('x_perner_id');

        $query  = $this->Data_model->load_roster_ta($data['date_1'], $data['x_perner_id']);
        $result = $query->result();
        
        $array_hari = array("1"=>"Minggu", "2"=>"Senin", "3"=>"Selasa", "4"=>"Rabu", "5"=>"Kamis", "6"=>"Jumat", "7"=>"Sabtu");
        
        if ($query->num_rows() > 0)
        {       
            $no = 1;
            
            foreach($result as $row_Recordset)
            {
                if ($row_Recordset->shifting_work_hour) {
                    list($jam_masuk, $jam_keluar) = explode("-", $row_Recordset->shifting_work_hour);
                } else {
                    $jam_masuk  = '';
                    $jam_keluar = '';
                }
                
                $data['list_jadwal_ta'][] =                 
                    array(
                        "no"            => $no++,
                        "tgl"           => $row_Recordset->tgl,
                        "hari"          => $array_hari[$row_Recordset->hari],
                        "perner_id"     => $row_Recordset->perner_id,
                        "shifting_code" => $row_Recordset->shifting_code,
                        "jam_masuk"     => trim($jam_masuk),
                        "ist_1_hour"    => $row_Recordset->ist_1_hour,
                        "ist_2_hour"    => $row_Recordset->ist_2_hour,
                        "ist_3_hour"    => $row_Recordset->ist_3_hour,
                        "jam_keluar"    => trim($jam_keluar),
                        "ws"            => $row_Recordset->ws,
                        "off_desc"      => $row_Recordset->off_desc,
                    );
            }           
        }
        else {
            $data['list_jadwal_ta'] = array();
        }
        $this->load->view('default', $data);
    }
}
