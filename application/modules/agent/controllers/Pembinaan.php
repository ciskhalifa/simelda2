
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Pembinaan extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email']) && $_SESSION['role'] == '4') {
            redirect('login');
        }
        date_default_timezone_set("Asia/Bangkok");

    }

    public function index()
    {
        $data['js'] = 'pembinaan/js';
        $data['css'] = 'pembinaan/css';
        $data['content'] = 'pembinaan/pembinaan';
        $data['kolom'] = array("Kode", "TL", "Jenis Bina", "Masalah", "Action Plan", "Bina", "Batas Verifikasi", "Status", "Opsi");
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vbina WHERE kode_agent=" . $_SESSION['kode'], 3);
        $this->load->view('default', $data);
    }

    public function updateBina()
    {
        if (IS_AJAX) {
            $arrdata['action_plan'] = $this->input->post('action_plan');
            $arrdata['approval_agent'] = '1';
            $this->Data_model->updateDataWhere($arrdata, 't_bina', array('kode' => $this->input->post('kode')));
            echo json_encode("ok");
        }
    }

    public function view_detail()
    {
        $id = $this->uri->segment(4);
        $kondisi = "kode";
        $data['rowdata'] = $this->Data_model->satuData('vbina', array($kondisi => $id));
        $this->load->view('pembinaan/view_detail', $data);
    }
}
