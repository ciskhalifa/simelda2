<link href="<?= base_url('assets/admin') ?>/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="<?= base_url('assets/admin/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.css') ?>" rel="stylesheet">

<style>
    .box-tools {
        position: absolute;
        right: 10px;
        top: 5px;
    }

    .col-label {
        position: relative;
        width: 100%;
        padding-right: .75rem;
    }

    div.col-label.col-md-5 {
        position: relative;
        width: 100%;
        padding-right: .75rem;
    }

    .rounded-circle-b {
        border-radius: 10% !important
    }

    .col-label-b {
        position: relative;
        width: 100%;
        padding-right: .75rem;
        font-weight: bold;
    }

    tr.hijau td {
    background-color : #e6ffb3;
    }

    tr.merah td {
        background-color : #FDEDEC; 
    }

    .table tbody tr:hover td, .table tbody tr:hover th {
        background-color: #D2FFF2;
        
    }
    
</style>