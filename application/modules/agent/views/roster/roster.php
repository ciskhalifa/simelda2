<div class="alert alert-danger alert-sm" role="alert">
    <marquee style="font-family:Arial; color: #A93226;"  scrollamount="5"><small>Fitur <b>roster</b> pada <b>Simelda</b> ini masih dalam tahap <b>percobaan</b> (<b>trial</b>), untuk itu TA/TL/BRS diharapkan <b>selalu crosscheck</b> dengan <b>roster</b> yang ada di <b>Garudazone</b></small></marquee>
</div>
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Roster TA</h1>
    <form class="form-inline" name="cms_filter" method="post" action="<?php echo $form_action; ?>">
      <div class="form-group mb-2">
        <label for="staticEmail2" class="sr-only">Bulan</label>
        <input name="date_1" id="date_1" type="text" class="form-control input-sm" value="<?php echo $date_1 = isset($date_1) ? $date_1 : '' ; ?>">
      </div>
      <div class="form-group mx-sm-3 mb-2">
        <label for="inputPassword2" class="sr-only">TA</label>
        <select name="x_perner_id" id="x_perner_id" class="form-control input-sm" name="size">
            <option value="">Silakan Pilih..</option>
            <?php 
                
                foreach ($list_sdm as $row) {
                    if ($x_perner_id == $row->perner_id){
                        $temp_perner_id = "selected=selected";
                    } else {
                        $temp_perner_id = '';
                    }
                    echo "<option value=".$row->perner_id." $temp_perner_id>".$row->sdm_name."</option>";
                }
            ?>
        </select>
      </div>
      <div class="form-group mx-sm-2 mb-2">
       <button id="ok" type="submit" class="btn btn-success">Lihat</button>
      </div>
    </form>
    <div id="tabelpegawai" class="slideInDown animated--grow-in" data-appear="appear" data-animation="slideInDown">
        <div class="content-detached">
            <div class="content-body">
                <section class="row">
                    <div class="col-md-12">
                        <div class="card shadow mb-2">
                            <div class="card-header py-3">
                                <h4 class="m-0 font-weight-bold text-primary"> Jadwal Kerja </h4>
                                <a class="heading-elements-toggle"><i class="icon-arrow-right-4"></i></a>
                            </div>
                            <div class="card-body">
                                <table id="PembinaanTable" class="table table-white-space table-bordered ">
                                    <thead>
                                        <tr>
                                            <?php
                                            if ($kolom) {
                                                foreach ($kolom as $key => $value) {
                                                    if (strlen($value) == 0) {
                                                        echo '<th data-type="numeric"></th>';
                                                    } else {
                                                        echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php                   
                                        if (count($list_jadwal_ta) > 0)
                                        {
                                            $no = 1;
                                            
                                            $now = date("Y-m-d", time());
                                            
                                            foreach($list_jadwal_ta as $id => $rows)
                                            {
                                                extract($rows, EXTR_OVERWRITE);
                                                
                                                if ($now == $tgl) {
                                                    $style = 'class="hijau"';
                                                } else {
                                                    
                                                    if ($hari == 'Sabtu' || $hari == 'Minggu') {
                                                        $style = 'class="merah"';
                                                    } else {
                                                        $style = '';
                                                    }
                                                }
                                                                            
                                                echo "
                                                    <tr $style>
                                                        <td class=\"text-center\">$tgl</td>
                                                        <td class=\"text-center\">$hari</td>
                                                        <td class=\"text-center\">$perner_id</td>
                                                        <td class=\"text-center\">$shifting_code</td>
                                                        <td class=\"text-center\"><b>$jam_masuk</b></td>
                                                        <td class=\"text-center\">$ist_1_hour</td>
                                                        <td class=\"text-center\">$ist_2_hour</td>
                                                        <td class=\"text-center\">$ist_3_hour</td>
                                                        <td class=\"text-center\"><b>$jam_keluar</b></td>
                                                        <td class=\"text-center\">$ws</td>
                                                        <td class=\"text-center\">$off_desc</td>
                                                    </tr>";
                                                
                                                $no++;
                                            }
                                        }
                                        
                                        ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">Hari</th>
                                        <th class="text-center">Perner ID</th>
                                        <th class="text-center">Shift</th>
                                        <th class="text-center">Login</th>
                                        <th class="text-center">Ist 1</th>                      
                                        <th class="text-center">Ist 2</th>
                                        <th class="text-center">Ist 3</th>
                                        <th class="text-center">Logout</th>
                                        <th class="text-center">WS</th>
                                        <th class="text-center">Keterangan</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>