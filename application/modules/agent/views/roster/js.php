<!-- Latest compiled and minified JavaScript -->
<script src="<?= base_url() ?>assets/admin/js/formValidation.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/validator.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.isloading.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/select2/select2.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/bootstrap-datetimepicker/moment.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script>
    $(function() {
        $("#PembinaanTable").dataTable({
        	paging: false,
			sort: false,
			searching: false,
			info: false,
			ordering: false,
			"oLanguage": {
        		"sEmptyTable": "Jadwal Tidak Tersedia"
    		}
        });
        $("#date_1").datetimepicker({
		    viewMode: 'years',
			format: 'YYYY-MM',
			icons: {
				previous: 'fa fa-chevron-left',
				next: 'fa fa-chevron-right',
				today: 'fa fa-screenshot',
			}
		});
    });
</script>