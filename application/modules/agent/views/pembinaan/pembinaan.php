<div class="container-fluid">
    <div id="tabelpegawai" class="slideInDown animated--grow-in" data-appear="appear" data-animation="slideInDown">
        <div class="content-detached">
            <div class="content-body">
                <section class="row">
                    <div class="col-md-12">
                        <div class="card shadow mb-2">
                            <div class="card-header py-3">
                                <h4 class="m-0 font-weight-bold text-primary"> List <?= $this->uri->segment('2'); ?></h4>
                                <a class="heading-elements-toggle"><i class="icon-arrow-right-4"></i></a>
                            </div>
                            <div class="card-body">
                                <table id="PembinaanTable" class="table table-white-space table-bordered ">
                                    <thead>
                                        <tr>
                                            <?php
                                            if ($kolom) {
                                                foreach ($kolom as $key => $value) {
                                                    if (strlen($value) == 0) {
                                                        echo '<th data-type="numeric"></th>';
                                                    } else {
                                                        echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rowdata as $row) : ?>
                                            <tr>
                                                <td><?= $row->kode; ?></td>
                                                <td><?= $row->tl; ?></td>
                                                <td><?= $row->nama_jenis; ?></td>
                                                <td><?= $row->masalah; ?></td>
                                                <td><?= $row->action_plan; ?></td>
                                                <td><?= date('d F Y', strtotime($row->tgl_data)); ?></td>
                                                <td><?= date('d F Y', strtotime($row->tgl_verifikasi)); ?></td>
                                                <td><?= $row->status; ?></td>
                                                <td>
                                                    <button type="button" data-status="<?= $row->status ?>" data-kode="<?= $row->kode ?>" class="btn btn-sm btn-warning edit" id="edit"><i class="fa fa-edit"></i></button>
                                                    <button type="button" data-status="<?= $row->status ?>" data-kode="<?= $row->kode ?>" class="btn btn-sm btn-info detail" id="detail"><i class="fa fa-eye"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div id="containerdetail" class="content-detached">
        <div class="content-body">
            <div id="contentdetail">
            </div>
        </div>
    </div>
</div>