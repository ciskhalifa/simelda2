<!-- Latest compiled and minified JavaScript -->
<script src="<?= base_url() ?>assets/admin/js/formValidation.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/validator.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.isloading.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jszip.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/pdfmake.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/vfs_fonts.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/select2/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>

<div id="myUpdate" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Action Plan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="action_plan">
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Action Plan</label>
                            <div class="col-md-8">
                                <textarea class="form-control" placeholder="Action Plan" name="action_plan" id="action_plan"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="kode">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnUpdate" class="btn btn-warning">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $("#PembinaanTable").dataTable();

        $(".detail").on('click', function() {
            $('#tabelpegawai').hide();
            $('#contentdetail').load('' + 'pembinaan/view_detail/' + $(this).attr("data-kode"));
            $('#containerdetail').fadeIn('fast');
        });

        $(".edit").on('click', function() {
            var status = $(this).data('status');
            if (status === '' || status === "VERIFIKASI") {
                $('#myUpdate').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $("#kode").val($(this).data('kode'));
            } else if (status == "SELESAI") {
                alert("Pembinaan sudah selesai");
            }
        });

        $("#btnUpdate").bind('click', function() {
            var link = "<?= base_url('agent/pembinaan/updateBina') ?>";
            $.ajax({
                url: link,
                type: "POST",
                data: "kode=" + $("#kode").val() + "&action_plan=" + $("#action_plan").val(),
                dataType: "html",
                success: function(html) {
                    notify("Update berhasil", "success")
                    $("#updateBina").modal("hide");
                    location.reload(true);
                }
            })
        });

    });
</script>