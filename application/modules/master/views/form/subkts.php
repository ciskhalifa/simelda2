<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $cid = ($aep == 'salin') ? '' : $rowdata->kode;
    $kode_kts = $rowdata->kode_kts;
    $kode_kategori = $rowdata->kode_kategori;
    $kode_verifikasi = $rowdata->kode_verifikasi;
    $nama_subkts = $rowdata->nama_subkts;
} else {
    $cid = "";
    $kode_kts = "";
    $kode_kategori = "";
    $kode_verifikasi = "";
    $nama_subkts = "";
}
?>
<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal" method="POST">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group row">
            <label class="col-md-2 label-control">Ketidaksesuaian</label>
            <div class="col-md-6">
                <select class="select2 form-control" name="kode_kts" id="kode_kts">
                    <option value="">- Pilihan -</option>
                    <?php
                    $n = (isset($rowdata->kode_kts)) ? $rowdata->kode_kts : '';
                    $q = $this->Data_model->selectData('m_kts', 'kode');
                    foreach ($q as $row) {
                        $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                        echo '<option data-id="' . $row->nama_kts . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->nama_kts . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Kategori</label>
            <div class="col-md-6">
                <select class="select2 form-control kts_kategori" name="kode_kategori" id="kode_kategori">
                    <option value="">- Pilihan -</option>
                    <?php
                    $n = (isset($rowdata->kode_kategori)) ? $rowdata->kode_kategori : '';
                    $q = $this->Data_model->selectData('m_kategori', 'kode');
                    foreach ($q as $row) {
                        $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                        echo '<option data-id="' . $row->nama_kategori . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->nama_kategori . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Deskripsi</label>
            <div class="col-md-6">
                <textarea class="form-control" placeholder="Deskripsi" name="nama_subkts" id="kts_desc"><?= $nama_subkts; ?></textarea>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Verifikasi</label>
            <div class="col-md-6">
                <select class="select2 form-control" name="kode_verifikasi" id="kode_verifikasi">
                    <option value="">- Pilihan -</option>
                    <?php
                    $n = (isset($rowdata->kode_verifikasi)) ? $rowdata->kode_verifikasi : '';
                    $q = $this->Data_model->selectData('m_verifikasi', 'kode');
                    foreach ($q as $row) {
                        $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                        echo '<option data-id="' . $row->nama_verifikasi . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->nama_verifikasi . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-actions">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<script>
    $(function() {
        $("#tmblBatal").on("click", function() {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function(c) {
            if (c.isDefaultPrevented()) {} else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = new FormData($('#xfrm')[0]);
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    cache: false,
                    contentType: false,
                    processData: false,
                    //dataType: "html",
                    beforeSend: function() {
                        $(".card #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: ''
                        })
                    },
                    success: function(d) {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function() {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>