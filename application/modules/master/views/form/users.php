<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $cid = ($aep == 'salin') ? '' : $rowdata->kode;
    $kode = $rowdata->kode;
    $fullname = $rowdata->fullname;
    $email = $rowdata->email;
    $password = $rowdata->password;
    $role = $rowdata->role;
    $is_active = $rowdata->is_active;
} else {
    $cid = "";
    $kode = "";
    $fullname = "";
    $email = "";
    $password = "";
    $role = "";
    $is_active = "";
}
?>
<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group row">
            <label class="col-md-2 label-control">Fullname</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Fullname" name="fullname" id="fullname" value="<?php echo $fullname; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Email</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Email" name="email" id="email" value="<?php echo $email; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Password</label>
            <div class="col-md-4">
                <input type="password" class="form-control input-sm" placeholder="Password" name="password" id="password" value="<?php echo $password; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Role</label>
            <div class="col-md-4">
                <select class="select2 form-control" name="role" id="role">
                    <option value="">- Pilihan -</option>
                    <option value="1" <?= ($role == 1) ? " selected= selected" : "" ?>> Super Admin </option>
                    <option value="21" <?= ($role == 21) ? " selected= selected" : "" ?>> SPV </option>
                    <option value="22" <?= ($role == 22) ? " selected= selected" : "" ?>> Admin </option>
                    <option value="23" <?= ($role == 23) ? " selected= selected" : "" ?>> QC </option>
                    <option value="3" <?= ($role == 3) ? " selected= selected" : "" ?>> Team Leader </option>
                    <option value="4" <?= ($role == 4) ? " selected= selected" : "" ?>> Agent </option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Is Active</label>
            <div class="col-md-4">
                <select class="select2 form-control" name="is_active" id="is_active">
                    <option value="">- Pilihan -</option>
                    <option value="0" <?= ($is_active == 0) ? " selected= selected" : "" ?>> Not Active </option>
                    <option value="1" <?= ($is_active == 1) ? " selected= selected" : "" ?>> Active </option>
                </select>
            </div>
        </div>
        <div class="form-actions">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<script>
    $(function() {
        $("#tmblBatal").on("click", function() {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function(c) {
            if (c.isDefaultPrevented()) {} else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = $("#xfrm").serialize();
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    dataType: "html",
                    beforeSend: function() {
                        $(".card #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: ''
                        })
                    },
                    success: function(d) {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function() {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>