<div id="divsatu" class="animated--grow-in">
    <input type="hidden" id="tabel" value="<?php echo $tabel; ?>">
    <input type="hidden" id="kolom" value="<?php echo $jmlkolom; ?>">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Tabel <?= strtoupper($this->uri->segment('3')); ?></h6>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="tmblTambah"><i class="fa fa-plus"></i></button>
            </div>
        </div>
        <div class="card-body">
            <table id="data-table-basic" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <?php
                        if ($kolom) {
                            foreach ($kolom as $key => $value) {
                                if (strlen($value) == 0) {
                                    echo '<th data-type="numeric"></th>';
                                } else {
                                    echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                }
                            }
                        }
                        ?>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<div id="divdua" class="content-detached content-right animated--grow-in" style="display:none">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h3 class="m-0 font-weight-bold text-primary">Form <?= strtoupper($this->uri->segment('3')); ?></h3>
        </div>
        <div class="card-body">
            <div id="divform"></div>
        </div>
    </div>
</div>

<script src="<?= base_url('assets/admin/js/jquery.isloading.min.js'); ?>"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/js/demo/datatables-demo.js"></script>
<script>
    $(function() {
        var myApp = myApp || {};
        if ($("#kolom").val() > 0) {
            loadTabelGlobalCIS($("#kolom").val(), "master", "");
        }
        $("#tmblTambah").on("click", function() {
            $("#divform").load("master/loadForm/" + $("#tabel").val() + "/-/");
            $("#divsatu").slideUp('fast');
            $("#divdua").slideDown('fast');
        });
        $("#tmblBatal").on("click", function() {
            $("#divdua").slideUp('fast');
            $("#divsatu").slideDown('fast');
            myApp.oTable.fnDraw(false);

        });
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>