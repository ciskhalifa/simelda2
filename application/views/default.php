<?php
$data['js'] = $js;
$data['css'] = $css;

$this->load->view(VIEW_INCLUDE . '/header', $data);
echo '<div id="wrapper">';
$this->load->view(VIEW_INCLUDE . '/sidebar');
echo '<div id="content-wrapper" class="d-flex flex-column">';
echo '<div id="content">';
$this->load->view(VIEW_INCLUDE . '/topbar');
$this->load->view($content);
echo '</div>';
echo '<footer class="sticky-footer bg-white"><div class="container my-auto"><div class="copyright text-center my-auto"><span>Copyright &copy; ' . APP_NAME . ' 2019</span></div></div></footer>';
echo '</div>';
$this->load->view(VIEW_INCLUDE . '/footer', $data);
echo '</div>';
