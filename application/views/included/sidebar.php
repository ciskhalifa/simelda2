<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('dashboard'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fab fa-accusoft"></i>
        </div>
        <div class=" sidebar-brand-text mx-3">Simelda <sup>v.1</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Administrator
    </div>

    <!-- SUPER ADMIN -->
    <?php if ($_SESSION['role'] == '1') : ?>
        <!-- ROLE SUPER ADMIN -->
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            Master
        </div>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('master') ?>">
                <i class="fas fa-fw fa-cog"></i>
                <span>Settings</span></a>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-fw fa-user"></i>
                <span>Logout</span></a>
        </li>
        <!-- END ROLE SUPER ADMIN -->
    <?php endif; ?>
    <!-- SUPER ADMIN -->

    <!-- SPV -->
    <?php if ($_SESSION['role'] == '21') : ?>
        <!-- ROLE ADMIN -->
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('spv'); ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <!-- Heading -->
        <div class="sidebar-heading">
            Pembinaan
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('spv/pembinaan'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Pembinaan</span></a>
        </li>
        <!-- END ROLE ADMIN -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('spv/rekomendasi'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Rekomendasi</span></a>
        </li>
        <!-- END ROLE ADMIN -->
    <?php endif; ?>
    <!-- SPV -->

    <!-- ADMIN -->
    <?php if ($_SESSION['role'] == '22') : ?>
        <!-- ROLE ADMIN -->
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('admin'); ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <!-- Heading -->
        <div class="sidebar-heading">
            Pembinaan
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('admin/pembinaan'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Pembinaan</span></a>
        </li>
        <!-- END ROLE ADMIN -->
    <?php endif; ?>
    <!-- ADMIN -->

    <!-- QC -->
    <?php if ($_SESSION['role'] == '23') : ?>
        <!-- ROLE QC -->
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('qc'); ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <!-- Heading -->
        <div class="sidebar-heading">
            Pembinaan
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('qc/pembinaan'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Pembinaan</span></a>
        </li>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('qc/rekomendasi'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Rekomendasi</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-wrench"></i>
                <span>Pemutihan</span>
            </a>
            <div id="collapseUtilities" class="collapse show" aria-labelledby="headingUtilities" data-parent="#accordionSidebar" style="">
                <div class="bg-white py-2 collapse-inner rounded">
                    <?php
                        $q = $this->Data_model->jalankanQuery("SELECT * FROM m_kategori", 3);
                        foreach ($q as $row) :
                            ?>
                        <a class="collapse-item" href="<?= base_url('pemutihan/' . str_replace(' ', '', $row->nama_kategori)); ?>"><?= $row->nama_kategori; ?></a>
                    <?php endforeach; ?>
                </div>
            </div>
        </li>
        <!-- END ROLE ADMIN -->
    <?php endif; ?>
    <!-- QC -->

    <!-- TEAM LEADER -->
    <?php if ($_SESSION['role'] == '3') : ?>
        <!-- ROLE TL -->
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('tl'); ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Pembinaan
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('tl/pembinaan'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Pembinaan</span></a>
        </li>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('tl/rekomendasi'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Rekomendasi</span></a>
        </li>
    <?php endif; ?>
    <!-- TEAM LEADER -->

    <!-- AGENT -->
    <?php if ($_SESSION['role'] == '4') : ?>
        <!-- ROLE AGENT -->
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('agent'); ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Pembinaan
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('agent/pembinaan'); ?>">
                <i class="fas fa-fw fa-user"></i>
                <span>Pembinaan</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-wrench"></i>
                <span>Roster</span>
            </a>
            <div id="collapseUtilities" class="collapse show" aria-labelledby="headingUtilities" data-parent="#accordionSidebar" style="">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="<?= base_url('agent/roster')?>">ROSTER TA</a>
                </div>
            </div>
        </li>
    <?php endif; ?>
    <!-- AGENT -->

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->