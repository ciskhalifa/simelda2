/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.1.38-MariaDB : Database - db_simelda2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_simelda2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_simelda2`;

/*Table structure for table `m_jenis` */

DROP TABLE IF EXISTS `m_jenis`;

CREATE TABLE `m_jenis` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(50) DEFAULT NULL,
  KEY `kode` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `m_jenis` */

insert  into `m_jenis`(`kode`,`nama_jenis`) values 
(1,'Coaching'),
(3,'Berita Acara Teguran Lisan'),
(4,'Surat Peringatan'),
(5,'Pengembalian ke PPJP'),
(2,'Counseling');

/*Table structure for table `m_kategori` */

DROP TABLE IF EXISTS `m_kategori`;

CREATE TABLE `m_kategori` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(50) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `m_kategori` */

insert  into `m_kategori`(`kode`,`nama_kategori`,`role`) values 
(1,'ROG','SPV'),
(2,'CASE','SPV'),
(3,'AUX','QC'),
(4,'NC Solusi','QC'),
(5,'Call Release','QC'),
(6,'Absensi','ADMIN'),
(7,'QM','QC'),
(8,'PNP','QC'),
(9,'MCI','QC');

/*Table structure for table `m_kts` */

DROP TABLE IF EXISTS `m_kts`;

CREATE TABLE `m_kts` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kts` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `m_kts` */

insert  into `m_kts`(`kode`,`nama_kts`) values 
(1,'Ketidaksesuaian Implementasi Tata Tertib Umum'),
(2,'Ketidaksesuaian Implementasi Ketentuan Umum'),
(3,'Ketidaksesuaian Jam Kerja'),
(4,'Ketidaksesuaian Pelayanan terhadap Penumpang'),
(5,'Ketidaksesuaian Target Kerja'),
(6,'Ketidaksesuaian Implementasi Tata Tertib Khusus (Attitude)'),
(7,'Kesalahan Informasi Kepada Pelanggan');

/*Table structure for table `m_subkts` */

DROP TABLE IF EXISTS `m_subkts`;

CREATE TABLE `m_subkts` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kts` int(11) DEFAULT NULL,
  `kode_kategori` int(11) DEFAULT NULL,
  `kode_verifikasi` int(11) DEFAULT NULL,
  `nama_subkts` text,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `m_subkts` */

insert  into `m_subkts`(`kode`,`kode_kts`,`kode_kategori`,`kode_verifikasi`,`nama_subkts`) values 
(1,3,6,1,'Keterlambatan < 10 Menit'),
(2,3,6,1,'Keterlambatan > 10 Menit'),
(3,3,3,1,'Kelebihan AUX (Tanpa Adjustment)'),
(4,4,4,1,'Ketidaksesuaian Informasi dan Penanganan Customer (NC Solusi dari QCO)');

/*Table structure for table `m_tingkat` */

DROP TABLE IF EXISTS `m_tingkat`;

CREATE TABLE `m_tingkat` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kode_jenis` int(11) DEFAULT NULL,
  `tingkat` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `m_tingkat` */

insert  into `m_tingkat`(`kode`,`kode_jenis`,`tingkat`) values 
(1,1,'T1'),
(2,1,'T2'),
(3,1,'T3'),
(4,2,'T4'),
(5,2,'T5'),
(6,2,'T6'),
(7,3,'BATL1'),
(8,3,'BATL2'),
(9,4,'SP1'),
(10,4,'SP2'),
(11,4,'SP3');

/*Table structure for table `m_users` */

DROP TABLE IF EXISTS `m_users`;

CREATE TABLE `m_users` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT 'default.jpg',
  `password` varchar(50) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT NULL,
  `tgl_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `m_users` */

insert  into `m_users`(`kode`,`fullname`,`email`,`image`,`password`,`role`,`is_active`,`tgl_data`) values 
(1,'Super Admin','superadmin@garudazone.com','default.jpg','$2y$10$0cGmDZd3gpYqTREB2tF6Gu9PHMsHIMUy0j2anHCJNc5',1,'1','2019-09-26 08:58:31'),
(2,'Nicke Ayu Claracitra','spv@garudazone.com','default.jpg','$2y$10$mg5SWavzDwbYHiqH7rNAoeXVLCE64p2Fpm63SrUx94r',21,'1','2019-09-26 09:35:33'),
(3,'Anwar','admin@garudazone.com','default.jpg','$2y$10$sAlcA9hodMuoY.pSejbJJeAdoMF8CFl72AOxkBHLzt0',22,'1','2019-09-26 09:42:45'),
(4,'Feddy','qc@garudazone.com','default.jpg','$2y$10$eGI224z7duHHtGG2QASzcOm.Ouo6ciAIV/UYjNyE1ou',23,'1','2019-09-26 09:43:21'),
(5,'Agi Sukma Handoko\r\n','tl1@garudazone.com','default.jpg','$2y$10$G3SybhLrMice9Vhid8jQRexjpL0STjwGWfdwkwpeZ/6',3,'1','2019-09-26 09:43:52'),
(6,'Ganjar Indramawan\r\n','tl2@garudazone.com','default.jpg','$2y$10$Q.4SpB..qiBjirOgO4hswemZBHLPwlJ4EYMunDXubTW',3,'1','2019-09-26 09:44:05'),
(7,'Agung Nurdhiansyah\r\n','tl3@garudazone.com','default.jpg','$2y$10$rVKgsRUjyNwrzR4ZmRfIwO.Xfa8lJSMxInqVmg4Lk5.',3,'1','2019-09-26 09:44:16'),
(8,'Yani Nurjanah','agent1@garudazone.com','default.jpg','$2y$10$qJqbwu38IOYfeDEvM8XOguKDW75EQToUcssfF8/kVCv',4,'1','2019-09-26 09:44:42'),
(9,'Mochamad Rangga Rahadian','agent2@garudazone.com','default.jpg','$2y$10$YjSIROeAwZyj7X/KOWD9puR154kB665TRpl.Zs7OiSB',4,'1','2019-09-26 09:44:52'),
(10,'Suci Fitriawati\r\n','agent3@garudazone.com','default.jpg','$2y$10$foNT7UZz0sY9XgpsMV3E4OH1TzUS5Ps./DkyXi6LFS7',4,'1','2019-09-26 09:45:04'),
(11,'Anita Maulinda Megasari','spv2@garudazone.com','default.jpg','$2y$10$mg5SWavzDwbYHiqH7rNAoeXVLCE64p2Fpm63SrUx94r',21,'1','2019-10-17 13:11:33');

/*Table structure for table `m_verifikasi` */

DROP TABLE IF EXISTS `m_verifikasi`;

CREATE TABLE `m_verifikasi` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `nama_verifikasi` varchar(20) DEFAULT NULL,
  `jumlah_hari` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `m_verifikasi` */

insert  into `m_verifikasi`(`kode`,`nama_verifikasi`,`jumlah_hari`) values 
(1,'1 Week',7),
(2,'1 Month',30),
(3,'6 Month',180);

/*Table structure for table `t_bina` */

DROP TABLE IF EXISTS `t_bina`;

CREATE TABLE `t_bina` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kode_agent` int(11) DEFAULT NULL,
  `kode_tl` int(11) DEFAULT NULL,
  `kode_spv` int(11) DEFAULT NULL,
  `kode_subkts` int(11) DEFAULT NULL,
  `tgl_kejadian` date DEFAULT NULL,
  `tgl_verifikasi` date DEFAULT NULL,
  `tingkatan` int(11) DEFAULT NULL,
  `masalah` text,
  `penyuluhan` text,
  `evidence` text,
  `action_plan` text,
  `hasil_verifikasi` text,
  `tgl_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approval_spv` datetime DEFAULT NULL,
  `approval_atasan` datetime DEFAULT NULL,
  `approval_agent` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `t_bina` */

insert  into `t_bina`(`kode`,`kode_agent`,`kode_tl`,`kode_spv`,`kode_subkts`,`tgl_kejadian`,`tgl_verifikasi`,`tingkatan`,`masalah`,`penyuluhan`,`evidence`,`action_plan`,`hasil_verifikasi`,`tgl_data`,`approval_spv`,`approval_atasan`,`approval_agent`,`status`,`view`) values 
(1,10,6,2,4,'2019-09-23','2019-09-30',9,'TA seharusnya melakukan verifikasi sesuai panduan solusi permintaan email tiket pada poin 2 sebelum mengirimkan e-tiket PNR R8Y6T6 dan R8YE6M, dikarenakan alamat email yang disampaikan pelanggan tidak terdapat di data reservasi tersebut.','TA seharusnya melakukan verifikasi sesuai panduan solusi permintaan email tiket pada poin 2 sebelum mengirimkan e-tiket PNR R8Y6T6 dan R8YE6M, dikarenakan alamat email yang disampaikan pelanggan tidak terdapat di data reservasi tersebut.','IN-aD','Test','melakukan verifikasi sesuai panduan solusi permintaan email tiket','2019-10-25 12:32:36','2019-10-28 14:50:19','2019-10-25 07:32:36','2019-10-30 13:36:36','VERIFIKASI',6),
(2,10,7,2,4,'2019-09-27','2019-10-04',4,'TA tidak tepat menjelaskan bahwa untuk untuk penerbangan tanggal 21 Desember dengan rute ke Singapore akan dikenakan tambahan Surcharge 160% dikarenakan termasuk Peak Season. ','Cek kembali ketentuan GarudaMiles Peak Season Miles Surcharge dikarenakan tambahan 160% Miles Surcharge dikenakan jika rutenya CGKGNS, CGKTJQ dan CGKSUB.','10459668',NULL,NULL,'2019-10-25 15:53:21','2019-10-28 08:47:00','2019-10-25 10:53:21',NULL,'VERIFIKASI',7),
(3,0,6,NULL,4,'2019-09-23','2019-09-30',9,'TA seharusnya melakukan verifikasi sesuai panduan solusi permintaan email tiket pada poin 2 sebelum mengirimkan e-tiket PNR R8Y6T6 dan R8YE6M, dikarenakan alamat email yang disampaikan pelanggan tidak terdapat di data reservasi tersebut.',NULL,'IN-aD',NULL,NULL,'2019-10-28 10:54:01',NULL,'2019-10-28 04:54:01',NULL,'VERIFIKASI',6);

/*Table structure for table `t_rekomendasi` */

DROP TABLE IF EXISTS `t_rekomendasi`;

CREATE TABLE `t_rekomendasi` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kode_agent` int(11) DEFAULT NULL,
  `kode_tl` int(11) DEFAULT NULL,
  `kode_spv` int(11) DEFAULT NULL,
  `kode_subkts` int(11) DEFAULT NULL,
  `tgl_kejadian` date DEFAULT NULL,
  `tgl_verifikasi` date DEFAULT NULL,
  `tingkatan` int(11) DEFAULT NULL,
  `masalah` text,
  `penyuluhan` text,
  `evidence` text,
  `action_plan` text,
  `hasil_verifikasi` text,
  `tgl_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approval_spv` datetime DEFAULT NULL,
  `approval_atasan` datetime DEFAULT NULL,
  `approval_agent` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `t_rekomendasi` */

insert  into `t_rekomendasi`(`kode`,`kode_agent`,`kode_tl`,`kode_spv`,`kode_subkts`,`tgl_kejadian`,`tgl_verifikasi`,`tingkatan`,`masalah`,`penyuluhan`,`evidence`,`action_plan`,`hasil_verifikasi`,`tgl_data`,`approval_spv`,`approval_atasan`,`approval_agent`,`status`,`view`) values 
(1,10,5,2,4,'2019-09-26','2019-10-03',1,'Pelanggan memberi informasi bahwa akan melakukan penerbangan membawa hand carry komputer. Di menit 04\'03\" pelanggan informasikan ukurannya 30 x 60 x 70 cm dan TA menjawab di call center tidak ada informasi ukurannya hanya besaran kg saja. Dan sampai akhir percakapan TA mengiyakan untuk membawa monitor ke cabin tanpa memberi informasi ukuran bagasi cabin yang terdapan di GAZone. ','Berilah informasi dengan tepat karena untuk pengecekan bagasi cabin, di call ada informasinya. TA bisa mengecek di PIN 102/2019-R10 GA BAGGAGE RULES ada informasi ukuran maksimal bagasi cabin. Jikapun TA tidak menangkap maksud pelanggan antara bagasi biasa atau cabin, seharusnya TA bisa menggali kebutuhan pelanggan lebih detail.','10322465',NULL,NULL,'2019-10-24 14:50:48',NULL,NULL,NULL,NULL,5),
(2,10,7,2,4,'2019-09-27','2019-10-04',3,'TA tidak tepat menjelaskan bahwa untuk untuk penerbangan tanggal 21 Desember dengan rute ke Singapore akan dikenakan tambahan Surcharge 160% dikarenakan termasuk Peak Season. ','Cek kembali ketentuan GarudaMiles Peak Season Miles Surcharge dikarenakan tambahan 160% Miles Surcharge dikenakan jika rutenya CGKGNS, CGKTJQ dan CGKSUB.','10459668',NULL,NULL,'2019-10-24 14:50:49',NULL,NULL,NULL,NULL,7),
(3,0,6,11,4,'2019-09-23','2019-09-30',4,'TA seharusnya melakukan verifikasi sesuai panduan solusi permintaan email tiket pada poin 2 sebelum mengirimkan e-tiket PNR R8Y6T6 dan R8YE6M, dikarenakan alamat email yang disampaikan pelanggan tidak terdapat di data reservasi tersebut.',NULL,'IN-aD',NULL,NULL,'2019-10-24 14:50:49',NULL,NULL,NULL,'PROSES',6);

/*Table structure for table `vbina` */

DROP TABLE IF EXISTS `vbina`;

/*!50001 DROP VIEW IF EXISTS `vbina` */;
/*!50001 DROP TABLE IF EXISTS `vbina` */;

/*!50001 CREATE TABLE  `vbina`(
 `kode` int(11) ,
 `agent` varchar(50) ,
 `kode_agent` int(11) ,
 `tl` varchar(50) ,
 `kode_tl` int(11) ,
 `kode_sub` int(11) ,
 `nama_subkts` text ,
 `tgl_kejadian` date ,
 `tgl_verifikasi` date ,
 `masalah` text ,
 `penyuluhan` text ,
 `evidence` text ,
 `action_plan` text ,
 `hasil_verifikasi` text ,
 `tgl_data` timestamp ,
 `approval_spv` datetime ,
 `approval_atasan` datetime ,
 `approval_agent` datetime ,
 `status` varchar(20) ,
 `nama_jenis` varchar(50) ,
 `kode_jenis` int(11) ,
 `nama_kategori` varchar(50) ,
 `role` varchar(50) ,
 `tingkat` varchar(10) ,
 `kode_tingkat` int(11) ,
 `view` int(11) ,
 `spv` varchar(50) ,
 `kode_spv` int(11) 
)*/;

/*Table structure for table `vrekomendasi` */

DROP TABLE IF EXISTS `vrekomendasi`;

/*!50001 DROP VIEW IF EXISTS `vrekomendasi` */;
/*!50001 DROP TABLE IF EXISTS `vrekomendasi` */;

/*!50001 CREATE TABLE  `vrekomendasi`(
 `kode` int(11) ,
 `agent` varchar(50) ,
 `kode_agent` int(11) ,
 `tl` varchar(50) ,
 `kode_tl` int(11) ,
 `kode_sub` int(11) ,
 `nama_subkts` text ,
 `tgl_kejadian` date ,
 `tgl_verifikasi` date ,
 `masalah` text ,
 `penyuluhan` text ,
 `evidence` text ,
 `action_plan` text ,
 `hasil_verifikasi` text ,
 `tgl_data` timestamp ,
 `approval_spv` datetime ,
 `approval_atasan` datetime ,
 `approval_agent` datetime ,
 `status` varchar(20) ,
 `nama_jenis` varchar(50) ,
 `kode_jenis` int(11) ,
 `nama_kategori` varchar(50) ,
 `role` varchar(50) ,
 `tingkat` varchar(10) ,
 `kode_tingkat` int(11) ,
 `view` int(11) ,
 `spv` varchar(50) ,
 `kode_spv` int(11) 
)*/;

/*View structure for view vbina */

/*!50001 DROP TABLE IF EXISTS `vbina` */;
/*!50001 DROP VIEW IF EXISTS `vbina` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vbina` AS (select `d`.`kode` AS `kode`,`a`.`fullname` AS `agent`,`a`.`kode` AS `kode_agent`,`b`.`fullname` AS `tl`,`b`.`kode` AS `kode_tl`,`c`.`kode` AS `kode_sub`,`c`.`nama_subkts` AS `nama_subkts`,`d`.`tgl_kejadian` AS `tgl_kejadian`,`d`.`tgl_verifikasi` AS `tgl_verifikasi`,`d`.`masalah` AS `masalah`,`d`.`penyuluhan` AS `penyuluhan`,`d`.`evidence` AS `evidence`,`d`.`action_plan` AS `action_plan`,`d`.`hasil_verifikasi` AS `hasil_verifikasi`,`d`.`tgl_data` AS `tgl_data`,`d`.`approval_spv` AS `approval_spv`,`d`.`approval_atasan` AS `approval_atasan`,`d`.`approval_agent` AS `approval_agent`,`d`.`status` AS `status`,`e`.`nama_jenis` AS `nama_jenis`,`e`.`kode` AS `kode_jenis`,`f`.`nama_kategori` AS `nama_kategori`,`f`.`role` AS `role`,`g`.`tingkat` AS `tingkat`,`g`.`kode` AS `kode_tingkat`,`d`.`view` AS `view`,`h`.`fullname` AS `spv`,`d`.`kode_spv` AS `kode_spv` from (((((((`t_bina` `d` left join `m_users` `a` on((`a`.`kode` = `d`.`kode_agent`))) left join `m_users` `b` on((`b`.`kode` = `d`.`kode_tl`))) left join `m_users` `h` on((`h`.`kode` = `d`.`kode_spv`))) left join `m_subkts` `c` on((`c`.`kode` = `d`.`kode_subkts`))) left join `m_tingkat` `g` on((`g`.`kode` = `d`.`tingkatan`))) left join `m_jenis` `e` on((`e`.`kode` = `g`.`kode_jenis`))) left join `m_kategori` `f` on((`f`.`kode` = `c`.`kode_kategori`)))) */;

/*View structure for view vrekomendasi */

/*!50001 DROP TABLE IF EXISTS `vrekomendasi` */;
/*!50001 DROP VIEW IF EXISTS `vrekomendasi` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vrekomendasi` AS (select `d`.`kode` AS `kode`,`a`.`fullname` AS `agent`,`a`.`kode` AS `kode_agent`,`b`.`fullname` AS `tl`,`b`.`kode` AS `kode_tl`,`c`.`kode` AS `kode_sub`,`c`.`nama_subkts` AS `nama_subkts`,`d`.`tgl_kejadian` AS `tgl_kejadian`,`d`.`tgl_verifikasi` AS `tgl_verifikasi`,`d`.`masalah` AS `masalah`,`d`.`penyuluhan` AS `penyuluhan`,`d`.`evidence` AS `evidence`,`d`.`action_plan` AS `action_plan`,`d`.`hasil_verifikasi` AS `hasil_verifikasi`,`d`.`tgl_data` AS `tgl_data`,`d`.`approval_spv` AS `approval_spv`,`d`.`approval_atasan` AS `approval_atasan`,`d`.`approval_agent` AS `approval_agent`,`d`.`status` AS `status`,`e`.`nama_jenis` AS `nama_jenis`,`e`.`kode` AS `kode_jenis`,`f`.`nama_kategori` AS `nama_kategori`,`f`.`role` AS `role`,`g`.`tingkat` AS `tingkat`,`g`.`kode` AS `kode_tingkat`,`d`.`view` AS `view`,`h`.`fullname` AS `spv`,`d`.`kode_spv` AS `kode_spv` from (((((((`t_rekomendasi` `d` left join `m_users` `a` on((`a`.`kode` = `d`.`kode_agent`))) left join `m_users` `b` on((`b`.`kode` = `d`.`kode_tl`))) left join `m_users` `h` on((`h`.`kode` = `d`.`kode_spv`))) left join `m_subkts` `c` on((`c`.`kode` = `d`.`kode_subkts`))) left join `m_tingkat` `g` on((`g`.`kode` = `d`.`tingkatan`))) left join `m_jenis` `e` on((`e`.`kode` = `g`.`kode_jenis`))) left join `m_kategori` `f` on((`f`.`kode` = `c`.`kode_kategori`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
